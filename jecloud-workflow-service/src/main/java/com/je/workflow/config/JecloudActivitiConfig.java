package com.je.workflow.config;

import com.google.common.base.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "bpm", ignoreUnknownFields = true)
public class JecloudActivitiConfig {

    private String cache;

    public String getCache() {
        if(Strings.isNullOrEmpty(cache)){
            return "";
        }
        return cache;
    }

    public void setCache(String cache) {
        this.cache = cache;
    }
}
