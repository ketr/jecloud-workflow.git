/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.filter;

import com.google.common.base.Strings;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.project.Project;
import com.je.common.base.util.ProjectContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import com.je.servicecomb.filter.AbstractHttpServerFilter;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.foundation.common.http.HttpStatus;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import org.apache.servicecomb.swagger.invocation.Response;

/**
 * 设置流程当前登录人信息
 */

public class WorkFlowSecurityUserServerFilter extends AbstractHttpServerFilter {

    public static final String X_AUTH_TOKEN = "authorization";

    public WorkFlowSecurityUserServerFilter() {
    }

    @Override
    public int getOrder() {
        return 103;
    }

    @Override
    public Response afterReceiveRequest(Invocation invocation, HttpServletRequestEx requestEx) {
        String token = invocation.getContext(X_AUTH_TOKEN);
        if (!Strings.isNullOrEmpty(token)) {
            Authentication.setAuthenticatedUser(SecurityUserHolder.getCurrentAccount());
            Project project = ProjectContextHolder.getProject();
            if (project != null) {
                com.je.bpm.engine.impl.identity.Project wfProject = new com.je.bpm.engine.impl.identity.Project();
                wfProject.setCode(project.getCode());
                wfProject.setId(project.getId());
                wfProject.setName(project.getName());
                wfProject.setOrgCode(project.getOrgCode());
                wfProject.setOrgId(project.getOrgId());
                wfProject.setOrgName(project.getOrgName());
                Authentication.setAuthenticatedUserProject(wfProject);
            }
        }
        return null;
    }

    private Response buildRejectResponse(String message) {
        return Response.create(new HttpStatus(200, "Unauthorized"), message);
    }
}
