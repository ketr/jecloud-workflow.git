package com.je.workflow.cache;

import com.je.bpm.engine.impl.persistence.deploy.ProcessDefinitionCacheEntry;
import com.je.bpm.engine.impl.persistence.deploy.ProcessRedisManager;
import com.je.workflow.config.JecloudActivitiConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ProcessRedisManagerImpl implements ProcessRedisManager {

    @Autowired
    private ProcessCache processCache;
    @Autowired
    private JecloudActivitiConfig jecloudActivitiConfig;

    @PostConstruct
    public void init() {
        clear();
    }

    @Override
    public boolean isFeatureEnabled() {
        //需要用yml文件获取配置
        if (jecloudActivitiConfig.getCache().equals("redis")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ProcessDefinitionCacheEntry get(String s) {
        return processCache.get(s);
    }

    @Override
    public boolean contains(String s) {
        return processCache.contains(s);
    }

    @Override
    public void add(String s, ProcessDefinitionCacheEntry processDefinitionCacheEntry) {
        processCache.add(s, processDefinitionCacheEntry);
    }

    @Override
    public void remove(String s) {
        processCache.remove(s);
    }

    @Override
    public void clear() {
        processCache.clear();
    }
}
