/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.cache;

import com.esotericsoftware.kryo.serializers.SerializeUtils;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.engine.impl.persistence.deploy.ProcessDefinitionCacheEntry;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.common.base.cache.AbstractHashCache;
import org.springframework.stereotype.Service;

@Service("processCache")
public class ProcessCache extends AbstractHashCache<byte[]> {

    private SerializeUtils serializeUtils;

    public ProcessCache() {
        serializeUtils = new SerializeUtils();
    }

    public static final String CACHE_KEY = "processCache";

    private final static String ACTIVITI_CACHE = "activiti_cache:";
    private final static String BPMN_CACHE = "bpmn_cache:";
    private final static String PROCESS_CACHE = "process_cache:";

    public ProcessDefinitionCacheEntry get(String id) {
        byte[] entry = getCacheValue(ACTIVITI_CACHE + id);
        byte[] bpmn = getCacheValue(BPMN_CACHE + id);
        byte[] process = getCacheValue(PROCESS_CACHE + id);
        if (entry == null) {
            return null;
        }
        return new ProcessDefinitionCacheEntry((ProcessDefinition) serializeUtils.deSerialize(entry)
                , (BpmnModel) serializeUtils.deSerialize(bpmn)
                , (Process) serializeUtils.deSerialize(process));

    }

    public void add(String id, ProcessDefinitionCacheEntry entry) {
        putCache(ACTIVITI_CACHE + id, serializeUtils.serialize(entry.getProcessDefinition()));
        putCache(BPMN_CACHE + id, serializeUtils.serialize(entry.getBpmnModel()));
        putCache(PROCESS_CACHE + id, serializeUtils.serialize(entry.getProcess()));
    }

    public void remove(String id) {
        removeCache(ACTIVITI_CACHE + id);
        removeCache(BPMN_CACHE + id);
        removeCache(PROCESS_CACHE + id);
    }

    public void clear() {
        super.clear();
    }

    public boolean contains(String id) {
        byte[] entry = getCacheValue(ACTIVITI_CACHE + id);
        if (entry == null) {
            return false;
        }
        return true;
    }

    @Override
    public String getCacheKey() {
        return CACHE_KEY;
    }

    @Override
    public boolean enableFristLevel() {
        return false;
    }

    @Override
    public String getName() {
        return "流程process缓存";
    }

    @Override
    public String getDesc() {
        return "流程process缓存！";
    }

}
