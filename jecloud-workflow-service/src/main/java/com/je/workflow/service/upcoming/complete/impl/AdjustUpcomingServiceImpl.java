/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 调整
 */
@Service
public class AdjustUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {

        DynaBean updateBean = new DynaBean();
        updateBean.setStr("TASK_HANDLE", "1");
        metaService.update(updateBean, ConditionsWrapper.builder().table("JE_WORKFLOW_RN_TASK")
                .in("TASK_ACTIVITI_TASK_ID", taskId.split(",")));

        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder().in("TASK_ACTIVITI_TASK_ID", taskId.split(","));
        List<DynaBean> dynaBeans = metaService.select("JE_WORKFLOW_RN_TASK",
                conditionsWrapper);
        List<String> userIds = new ArrayList<>();
        if (dynaBeans.size() > 0) {
            for (DynaBean dynaBean : dynaBeans) {
                userIds.add(dynaBean.getStr("ASSIGNEE_ID"));
            }
        }

        if (!userIds.isEmpty()) {
            for (String userId : userIds) {
                String associationId = workFlowUserService.getAssociationIdById(userId);
                pushService.pushRefresh(associationId);
            }
        }
    }
}
