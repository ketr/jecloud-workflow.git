/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user.userParser;

import com.google.common.base.Strings;
import com.je.bpm.core.model.config.task.assignment.AssigneeResource;
import com.je.bpm.core.model.config.task.assignment.BasicAssignmentConfigImpl;
import com.je.bpm.core.model.config.task.assignment.ProjectDepartmentAssignmentConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.model.dd.DicInfoVo;
import com.je.meta.rpc.dictionary.CustomBeanInvokeRpcService;
import com.je.meta.rpc.project.ProjectUserRpcService;
import com.je.servicecomb.RpcSchemaFactory;
import com.je.workflow.service.push.CommonSystemVariable;
import com.je.workflow.service.push.WorkFlowVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 产品部门
 */
public class UserProjectDepartmentParser extends AbstractUserParser {

    private MetaResourceService metaResourceService;

    @Override
    public JSONTreeNode parser(String userId,
                               BasicAssignmentConfigImpl basicAssignmentConfig, KaiteBaseUserTask kaiteBaseUserTask,
                               Boolean showCompany,
                               Map<String, Object> bean, String prod, Boolean multiple, Boolean addOwn, Map<String, String> customerMap) {

        DicInfoVo dicInfoVo = new DicInfoVo();
        Map<String, Object> customerVariables = new HashMap<>();
        ProjectDepartmentAssignmentConfigImpl projectDepartmentAssignmentConfig = (ProjectDepartmentAssignmentConfigImpl) basicAssignmentConfig;
        String sql = projectDepartmentAssignmentConfig.getSql();
        customerVariables.put("wfSql", sql);
        customerVariables.put("multiple", multiple);
        dicInfoVo.setCustomerVariables(customerVariables);

        return invokeRemoteService("meta", "JE_RBAC_PROJECT_WF_ROLE_TREE", "projectWorkflowOrgUserTreeService",
                "buildTree", dicInfoVo);

//        List<AssigneeResource> assigneeResourceList = basicAssignmentConfig.getResource();
//        List<String> projectDepartmentIds = new ArrayList<>();
//        for (AssigneeResource assigneeResource : assigneeResourceList) {
//            projectDepartmentIds.add(assigneeResource.getId());
//        }
//
//        List<String> userIds = getProjectUserIds(prod, projectDepartmentIds);
//        if (userId == null || userId.length() == 0) {
//            return null;
//        }
//        Object child = userManager.findUsers(userIds, showCompany, multiple, addOwn);
//        if (child != null && child instanceof JSONTreeNode) {
//            return (JSONTreeNode) child;
//        }
//        return null;
    }

    private JSONTreeNode invokeRemoteService(String product, String ddCode, String beanName, String method, DicInfoVo dicInfoVo) {
        CustomBeanInvokeRpcService customBeanInvokeRpcService =
                RpcSchemaFactory.getRemoteProvierClazz(product, "customBeanInvokeRpcService", CustomBeanInvokeRpcService.class);
        return customBeanInvokeRpcService.invoke(beanName, method, dicInfoVo);
    }

    @Override
    public Boolean analysis(String userId, BasicAssignmentConfigImpl basicAssignmentConfig, KaiteBaseUserTask kaiteBaseUserTask,
                            Map<String, Object> bean, String prod, Map<String, String> customerMap, String operationId) {
        List<AssigneeResource> assigneeResourceList = basicAssignmentConfig.getResource();
        List<String> projectDepartmentIds = new ArrayList<>();
        for (AssigneeResource assigneeResource : assigneeResourceList) {
            projectDepartmentIds.add(assigneeResource.getId());
        }
        if (projectDepartmentIds.isEmpty()) {
            return false;
        }
        String wfSql = "";
        if (basicAssignmentConfig.getPermission() != null) {
            if (!Strings.isNullOrEmpty(basicAssignmentConfig.getPermission().getSql())) {
                wfSql = basicAssignmentConfig.getPermission().getSql();
                wfSql = WorkFlowVariable.formatVariable(wfSql, convertToStringMap(bean));
                wfSql = CommonSystemVariable.formatVariable(wfSql);
            }
        }
        List<String> userIds = getProjectUserIds(prod, projectDepartmentIds, wfSql);

        if (userIds.contains(userId)) {
            return true;
        }
        return false;
    }

    private List<String> getProjectUserIds(String prod, List<String> projectDepartmentIds, String wfSql) {
        DynaBean dynaBean = getMetaResourceService().selectOneByNativeQuery(
                "JE_RBAC_PROJECTUSERMAPPING",
                NativeQuery.build().eq("PROJECTUSERMAPPING_PRODUCT_CODE", prod)
        );
        if (dynaBean == null) {
            return new ArrayList<>();
        }
        List<String> users = new ArrayList<>();
        //表
        String tableCode = dynaBean.getStr("PROJECTUSERMAPPING_TABLE_CODE");
        //部门外键字段code
        String deptFiledCode = dynaBean.getStr("PROJECTUSERMAPPING_DEPTIDFIELD_CODE");
        //用户id字段
        String userIdFiledCode = dynaBean.getStr("PROJECTUSERMAPPING_SYSUSERPKFIELD_CODE");
        //用户id字段
        String sql = dynaBean.getStr("PROJECTUSERMAPPING_GLSQL");
        if (Strings.isNullOrEmpty(sql)) {
            sql = wfSql;
        } else {
            sql += " " + wfSql;
        }
        ProjectUserRpcService projectUserRpcService = "workflow".equals(prod) ?
                SpringContextHolder.getBean(ProjectUserRpcService.class) :
                RpcSchemaFactory.getRemoteProvierClazz(prod, "projectUserRpcService", ProjectUserRpcService.class);

        List<DynaBean> projectUsers = projectUserRpcService.findProjectUserByDepartment(tableCode, deptFiledCode, projectDepartmentIds, sql);
        for (DynaBean user : projectUsers) {
            users.add(user.getStr(userIdFiledCode));
        }
        return users;
    }

    private MetaResourceService getMetaResourceService() {
        if (metaResourceService != null) {
            return metaResourceService;
        }
        metaResourceService = SpringContextHolder.getBean(MetaResourceService.class);
        return metaResourceService;
    }

    private static Map<String, String> convertToStringMap(Map<String, Object> originalMap) {
        Map<String, String> stringMap = new HashMap<>();

        for (Map.Entry<String, Object> entry : originalMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            // 转换为字符串
            String stringValue = (value != null) ? value.toString() : null;

            // 放入新的 Map
            stringMap.put(key, stringValue);
        }

        return stringMap;
    }


}
