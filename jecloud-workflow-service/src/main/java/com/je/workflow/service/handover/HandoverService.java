package com.je.workflow.service.handover;

import com.je.ibatis.extension.plugins.pagination.Page;

public interface HandoverService {
    Page load(String userId);

    void processAllHandover(String assignee, String toAssignee);

    void processHandoverByIds(String assignee, String toAssignee, String ids);

}
