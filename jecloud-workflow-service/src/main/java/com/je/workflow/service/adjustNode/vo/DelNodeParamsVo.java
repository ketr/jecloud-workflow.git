package com.je.workflow.service.adjustNode.vo;

/**
 * 添加节点参数 vo
 */
public class DelNodeParamsVo {

    public static DelNodeParamsVo build() {
        return new DelNodeParamsVo();
    }

    /**
     * 流程部署id
     */
    private String pdid;
    /**
     * 流程实例id
     */
    private String piid;
    /**
     * 业务主键
     */
    private String beanId;
    /**
     * 删除的节点id
     */
    private String taskNodeIds;


    public DelNodeParamsVo pdid(String pdid) {
        this.pdid = pdid;
        return this;
    }

    public DelNodeParamsVo beanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public DelNodeParamsVo taskNodeIds(String taskNodeIds) {
        this.taskNodeIds = taskNodeIds;
        return this;
    }

    public String getPiid() {
        return piid;
    }

    public void setPiid(String piid) {
        this.piid = piid;
    }

    public String getPdid() {
        return pdid;
    }

    public void setPdid(String pdid) {
        this.pdid = pdid;
    }

    public String getBeanId() {
        return beanId;
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    public String getTaskNodeIds() {
        return taskNodeIds;
    }


}
