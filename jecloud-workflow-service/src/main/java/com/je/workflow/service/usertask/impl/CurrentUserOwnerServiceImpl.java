/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask.impl;

import com.google.common.base.Strings;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.mapper.query.Condition;
import com.je.common.base.mapper.query.ConditionEnum;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.workflow.service.usertask.CurrentUserParam;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * 我的
 */
@Service(value = "upcomingOwnerService")
public class CurrentUserOwnerServiceImpl extends AbstractCurrentUserService {

    @Override
    public Map<String, Object> getTask(CurrentUserParam upcomingParam) {
        ConditionsWrapper conditionsWrapper = buildConditionsWrapper(upcomingParam);
        if (Strings.isNullOrEmpty(upcomingParam.getEnd()) || upcomingParam.getEnd().equals("0")) {
            return getRunTask(conditionsWrapper, upcomingParam);
        } else {
            return getEndTask(conditionsWrapper, upcomingParam);
        }
    }

    @Override
    public Long getBadge(CurrentUserParam upcomingParam) {
        ConditionsWrapper conditionsWrapper = buildConditionsWrapper(upcomingParam);
        buildRunApprovedWrapper(conditionsWrapper);
        return abstractGetRunTaskBadge("JE_WORKFLOW_V_RN_TASK", conditionsWrapper);
    }

    private Map<String, Object> getRunTask(ConditionsWrapper conditionsWrapper, CurrentUserParam upcomingParam) {
        buildRunApprovedWrapper(conditionsWrapper);
        return abstractGetRunTask("JE_WORKFLOW_V_RN_TASK", conditionsWrapper, upcomingParam.getPage());
    }

    private void buildRunApprovedWrapper(ConditionsWrapper conditionsWrapper) {
        //已经办理
        conditionsWrapper.eq("TASK_HANDLE", "1");
        //我发起的

        List<Condition> conditions = new ArrayList<>();
        Condition condition = new Condition();
        condition.setCn("and");
        condition.setCode("EXECUTION_STARTER");
        condition.setValue(Authentication.getAuthenticatedUser().getDeptId());
        condition.setType(ConditionEnum.EQ.getType());

        Condition condition2 = new Condition();
        condition2.setCn("AND");
        condition2.setCode("ASSIGNEE_ID");
        condition2.setType(ConditionEnum.EQ.getType());
        condition2.setValue(Authentication.getAuthenticatedUser().getDeptId());
        conditions.add(condition);
        conditions.add(condition2);

        Consumer<ConditionsWrapper> tConsumer = i -> {
            for (Condition c : conditions) {
                c.setCn("or");
                queryBuilderService.condition(i, c, null);
            }
        };
        conditionsWrapper.and(tConsumer);

    }

    private Map<String, Object> getEndTask(ConditionsWrapper conditionsWrapper, CurrentUserParam upcomingParam) {
        buildEndApprovedWrapper(conditionsWrapper);
        return abstractGetEndTask(conditionsWrapper, upcomingParam.getPage());
    }

    private void buildEndApprovedWrapper(ConditionsWrapper conditionsWrapper) {
        //我发起的
        conditionsWrapper.eq("EXECUTION_STARTER", Authentication.getAuthenticatedUser().getDeptId());
        //分组
        if (!JEDatabase.getCurrentDatabase().equalsIgnoreCase("ORACLE")) {
            conditionsWrapper.groupBy("EXECUTION_PIID");
        }
        //排序
        conditionsWrapper.orderByDesc("COLLECT_SY_CREATETIME", "SY_CREATETIME");
    }


}
