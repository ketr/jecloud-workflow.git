package com.je.workflow.service.adjustNode.vo;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.engine.impl.identity.Authentication;

import java.util.List;

/**
 * xml对象转成节点对象使用
 */
public class NodeInfoVo extends AbstractElementInfoVo {

    public static final String TYPE = "node";

    /**
     * 名称
     */
    private String name;
    /**
     * 宽
     */
    private String width;
    /**
     * 高
     */
    private String height;
    /**
     * 出线 id
     */
    private List<String> targets;

    public NodeInfoVo() {
        super(TYPE);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }


    /**
     * 创建一个json对象的task节点，需要和前端json保持一致
     *
     * @param id                节点id
     * @param x                 x轴坐标
     * @param y                 Y轴坐标
     * @param targetIds         出线ids
     * @param addSerialUserInfo 处理人信息
     * @return
     */
    public static JSONObject getTaskUserJson(String id, String x, String y, List<String> targetIds, AdjustSerialUserInfoVo addSerialUserInfo) {
        JSONObject user = new JSONObject();
        user.put("child", new JSONArray());
        JSONObject bounds = new JSONObject();
        bounds.put("x", x);
        bounds.put("y", y);
        bounds.put("width", 80);
        bounds.put("height", 40);
        user.put("bounds", bounds);
        user.put("resourceId", id);
        user.put("childShapes", new JSONArray());
        JSONObject stencil = new JSONObject();
        stencil.put("id", "task");
        user.put("stencil", stencil);
        JSONArray targets = new JSONArray();

        for (String targetId : targetIds) {
            JSONObject target = new JSONObject();
            target.put("resourceId", targetId);
            targets.add(target);
        }
        user.put("target", targets);

        JSONObject properties = new JSONObject();
        properties.put("categorydefinition", "kaiteUserTask");
        properties.put("name", addSerialUserInfo.getResourceName());
        properties.put("formSchemeName", "");
        properties.put("formSchemeId", "");
        properties.put("listSynchronization", "0");
        properties.put("retrieve", "0");
        properties.put("urge", "0");
        properties.put("invalid", "0");
        properties.put("transfer", "0");
        properties.put("delegate", "0");
        properties.put("formEditable", "0");
        properties.put("remind", "0");
        properties.put("simpleApproval", "0");
        properties.put("selectAll", "0");
        properties.put("asynTree", "0");
        properties.put("isJump", "0");
        properties.put("logicalJudgment", "0");
        properties.put("countersign", "0");
        properties.put("signBack", "0");
        properties.put("staging", "0");
        properties.put("initiatorCanCancel", "0");
        properties.put("initiatorCanUrged", "0");
        properties.put("initiatorInvalid", "0");
        properties.put("securityEnable", "0");
        properties.put("isCs", "1");
        properties.put("createCsUserId", Authentication.getAuthenticatedUser().getDeptId());
        properties.put("securityEnable", "0");
        properties.put("securityCode", "0");
        user.put("properties", properties);

        JSONObject assignmentConfig = new JSONObject();
        JSONArray assignmentResources = new JSONArray();
        JSONObject assignmentResource = new JSONObject();
        assignmentResource.put("entryPath", "");
        assignmentResource.put("type", "userConfig");
        assignmentResource.put("name", "按人员处理");
        assignmentResource.put("resourceName", addSerialUserInfo.getResourceName());
        assignmentResource.put("resourceCode", addSerialUserInfo.getResourceCode());
        assignmentResource.put("resourceId", addSerialUserInfo.getResourceId());
        assignmentResource.put("id", addSerialUserInfo.getId());
        assignmentResource.put("__action__", "doInsert");
        assignmentResource.put("_X_ROW_KEY", "row_4208");
        assignmentResource.put("resourceIcon", "");
        assignmentResource.put("permission", new JSONObject());
        assignmentResource.put("sql", "");
        assignmentResource.put("serviceName", "");
        assignmentResource.put("methodName", "");
        assignmentResources.add(assignmentResource);
        assignmentConfig.put("referToCode", "LOGUSER");
        assignmentConfig.put("resource", assignmentResources);
        user.put("assignmentConfig", assignmentConfig);
        user.put("commitBreakdown", new JSONArray());

        JSONObject dismissConfig = new JSONObject();
        dismissConfig.put("enable", "0");
        dismissConfig.put("dismissTaskNames", "");
        dismissConfig.put("dismissTaskIds", new JSONArray());
        dismissConfig.put("commitBreakdown", new JSONArray());
        dismissConfig.put("directSendAfterDismiss", "0");
        dismissConfig.put("forceCommitAfterDismiss", "0");
        dismissConfig.put("disableSendAfterDismiss", "0");
        dismissConfig.put("noReturn", "0");
        dismissConfig.put("directSendAfterReturn", "0");
        user.put("dismissConfig", dismissConfig);

        JSONObject earlyWarningAndPostponement = new JSONObject();
        earlyWarningAndPostponement.put("enable", "0");
        earlyWarningAndPostponement.put("processingTimeLimitDuration", "");
        earlyWarningAndPostponement.put("processingTimeLimitUnitName", "");
        earlyWarningAndPostponement.put("processingTimeLimitUnitCode", "");
        earlyWarningAndPostponement.put("warningTimeLimitDuration", "");
        earlyWarningAndPostponement.put("warningTimeLimitUnitName", "");
        earlyWarningAndPostponement.put("warningTimeLimitUnitCode", "");
        earlyWarningAndPostponement.put("reminderFrequencyDuration", "");
        earlyWarningAndPostponement.put("reminderFrequencyUnitName", "");
        earlyWarningAndPostponement.put("reminderFrequencyUnitCode", "");
        earlyWarningAndPostponement.put("resource", new JSONArray());
        user.put("earlyWarningAndPostponement", earlyWarningAndPostponement);

        JSONObject passRoundConfig = new JSONObject();
        passRoundConfig.put("enable", "0");
        passRoundConfig.put("auto", "0");
        passRoundConfig.put("circulationRules", new JSONArray());

        user.put("passRoundConfig", passRoundConfig);
        user.put("buttonsConfig", new JSONArray());
        user.put("customEventListeners", new JSONArray());

        JSONObject addSignature = new JSONObject();
        addSignature.put("enable", "0");
        addSignature.put("unlimited", "0");
        addSignature.put("notCountersigned", "0");
        addSignature.put("mandatoryCountersignature", "0");
        addSignature.put("circulationRules", new JSONArray());
        user.put("addSignature", addSignature);

        JSONObject approvalNotice = new JSONObject();
        approvalNotice.put("processInitiator", "0");
        approvalNotice.put("thisNodeApproved", "0");
        approvalNotice.put("thisNodeApprovalDirectlyUnderLeader", "0");
        approvalNotice.put("thisNodeApprovalDeptLeader", "0");
        user.put("approvalNotice", approvalNotice);

        JSONObject formConfig = new JSONObject();
        formConfig.put("fieldAssignment", new JSONObject());
        formConfig.put("fieldControl", new JSONObject());
        formConfig.put("taskFormButton", new JSONObject());
        formConfig.put("taskChildFunc", new JSONObject());
        user.put("formConfig", formConfig);
        return user;
    }


}
