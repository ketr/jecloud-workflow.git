package com.je.workflow.service.adjustNode;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.workflow.service.adjustNode.vo.AbstractElementInfoVo;
import com.je.workflow.service.adjustNode.vo.AdjustSerialUserInfoVo;
import com.je.workflow.service.adjustNode.vo.LineInfoVo;
import com.je.workflow.service.adjustNode.vo.NodeInfoVo;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultElement;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.*;

public class ConXmlToModel {

    public static String addTaskNode(InputSource in, String toTaskId, String toTargetId, String addTaskNodeName,
                                     String addTaskNodeId, String taskType) {
        // 创建 SAXReader 对象
        SAXReader reader = new SAXReader();
        // 读取 XML 文件
        Document document = null;
        try {
            document = reader.read(in);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        // 获取根元素
        Element root = document.getRootElement();
        //加签节点
        createTaskNode(root.element("root"), toTaskId, toTargetId, addTaskNodeName, addTaskNodeId, taskType);
        return document.asXML();
    }

    private static void createLineNode(Element rootMxCellElement, String toTaskId, String addTaskNodeId) {
        Element lineElement = new DefaultElement("mxCell");
        lineElement.addAttribute("id", "line" + ConXmlToModel.getUuid());
        lineElement.addAttribute("style", "edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0;entryY=0.5;entryDx=0;entryDy=0;");
        lineElement.addAttribute("parent", "1");
        lineElement.addAttribute("source", toTaskId);
        lineElement.addAttribute("target", addTaskNodeId);
        lineElement.addAttribute("edge", "1");
        lineElement.addAttribute("nodeType", "line");
        Element mxGeometryElement = new DefaultElement("mxGeometry");
        mxGeometryElement.addAttribute("relative", "1");
        mxGeometryElement.addAttribute("as", "geometry");
        lineElement.add(mxGeometryElement);
        rootMxCellElement.add(lineElement);
    }

    private static void createTaskNode(Element rootMxCellElement, String toTaskId, String toTargetId, String addTaskNodeName,
                                       String addTaskNodeId, String taskType) {
        Element element = getElementById(rootMxCellElement, toTaskId);
        String x = element.attributeValue("x", "0");
        String y = element.attributeValue("y", "0");

        // 创建新节点
        Element taskElement = new DefaultElement(taskType);
        taskElement.addAttribute("label", addTaskNodeName);
        taskElement.addAttribute("name", addTaskNodeName);
        taskElement.addAttribute("type", taskType);
        taskElement.addAttribute("id", addTaskNodeId);

        Element mxCellElement = new DefaultElement("mxCell");
        if (taskType.equals("batchtask")) {
            mxCellElement.addAttribute("style", "image;image=data:image/png,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAELhJREFUeNrsXU2MHMUZ7Z71Gtsg4gBRkPjREpARJNKOFVlKzMG7ciROlhdFgkMOnj04UnKxreRCpAijSMklke1LOHDY8REkxFqckIK8ewAOjuTxgSCsII9igkACs3HANv6B9JutjntnuqqrZqp/quo9aTSrXW1PT/V79f3UV1/FEWEFB7uX58SP6fuezJ/nxrzsSubn1ezvXu7cvcJRnxwxh8CY6DPJW1u8QPIZ8aoDffGCOHp4JcLo8ylRALZndrxmxfv2ht/ymrAS5/BOS0EBjDPDL4jZ3QXC6woCVmKZFoICyCN9W5B+v3BtfAZcpVNCDD0KgDP9gQBIrxLDyZAtQxwg8Ttipl+g7duAZViGRAhdCsDP2R7EP+SBT19FzHAieXVDsApxAMR/QZCfMAeswYs+CyH2lPhzYranm2PPPTrhY0o19oz4CGaPReOvvBJqQABHfMoexZ4Qn64OXaPwBJAQHwHtYUF+onq8mLyOJ0JYowCqJ39HuDvM6tSLNeEWdSmA6tydJfr5jYwPFl1zi1qOkf9o8naB5G8k8EwuiGdEC2CZ+G0x67fJMyfQE9ag8dmilgPkR5B7muR3CnhWp8WzowUYk/jbxazPxSy3sSyswRoFYObyvB7Vt9OKsAsExs800SVqNZD8MJtnSX6vgGd5tokuUdww8sPl6ZAvXgNVposUwKi/z0A3HMAVmm9CXNBqAPnbwuUh+cNBW7hEtT/zuAHkx8zPcoYwsSYsQW3BcatG8i+Q/MFj4PoKLoRjAUQh2xKfP5HBYh0FdTHJT4QsgpjkJ0IWQUzyEyGLIK6I/AhyXudzJQyA0oll5wXAVCcxJipJkcYkPxGyCOISyQ/Ss6iNmBT95LWzrLKJMhfCTpP8hAXMCC5FzghAVHWytoewhbbgVPMFIGq+O3xmhGV0ythPEFsmf1rZSRBlYafNoDi2SH4GvYRzQbFNF2iJ5CcqCoqtxQNWBCB8M3ZvIKrCgq14ILZAfi52EXXAyiKZDQuwRPITNWC7DVdoIgGIPpDM9xN1oT1pL9J4AvIjGLnAZ0A0AI+M25V6EgvA2n6iKRibi2MJQGxumeO4Ew3BnOBk+S6QWPC6wMCXaBjWhCtktEA2jgU4TPITDUR6Xlx5FoCBL+FbQGxqAXgaI9F0GHFU2wKw0pNwCNoVoyYW4BjHlXAE2lzVEkAy+89FTHsS7mBOcNaaBaDvT7iGQ1ZiAGZ+CIdRmBHSsQCc/QlXUcjdmLM/EbIVKLIAHY4f4Tg6k7hAhzh+k+Ghe1ochAYHw7HC/YFyWPJsQPT2w9PR49+fih5Mft62OX9oz39yK7p46Vb0wae3ot6/bnLgqoG03fomxT/t57ipAZL/9LHp6GdPTEf33qU30++4f2rw2vtkFF25/u1ABG+cux59/uU3HNDyAC53tS0Ag99i7H1yc7RvdrN0pjfFO/+8Eb165uuBKIjqgmGZBWCLEwkw0/96fqt13353YknaD2+Kum9fo2tUDsDp47pB8AGO1yhA0N/v21ZaYAtrAnE9t+sO7bjj8cSdIrRwQMsFYtWnfIbuPLWlss+DSwRrIMNvn942iCVSwGr0Lt4c/B8hxUiV6Ca6P+WQH9ke+PMXv/gm2jaN2XoqcZ9i7WAZnwnkiQB/2zE088M64YW45JUklqAbJXWDCgXA7M+Qm/GspksC0r3z4Q0l+SAAEFUncwSin//01sisrvq/NEbBPUA8DKpHuH1U6gIx+zPqk8PnLyIqZvulhGymqUzdTNIf3vgqunjpmw2zPUheBNzPX09f3fC/xMZsUIvujxwgZxH54W78+c0rY+Xx3/rH9YTcVwoJ2nlq64ilwf8WAff+m6e3cTVawfHhkdnD8bnt+mCGVgEuhg4Ri2bpv7ypFgHuJY0JssL75cn/DmZ4VeAL6wIR2Fqv8AB7VAKY4/is47ldWwrJbyvjAj+9SASwRrK4A/fy/GtfDVwxlQiIUY63Mv4//sB+P8J12KHIr2PWt51uhAgwm8uC1jR4VlkSuGKy+4IV2de+gw834Xh2u2SLs/8okKFREQ21O2Wg6Nq7H50uvIbKLdubfC+6Qhu5nhXALMfldpZFBhC0zNQiyCsLqFX3NRwf5KViQf6iuCYQzNICKNwfWeYHxK9ipfVv78s/Q7f0QbYGsPeJaT7kYQsg8v/0/wsI9m5FZQaqz9lxv54VAPnfyhESrICuJfE8DpjJWgDO/hkLIAM2sVSBQQmFJCP00Hf1c/pwp/KsADbtEOsnG6WjOcPxKCaHLM1YBs5/kl9OYRLEgvx596xrRUISABfANAlV2WdJvKCthlmcPKvFleHbnKcFaCD5AVkmyJS8H126xYeXjxkKgME+BaDbRDR0VL2AJAvGq4xDfAe4T2fQwNXR3cxiRQB3liu4DyikKHWBaAEywA4uGaoMHrGDLA+fGZZdM+OjBC3AiAAUQWP7oWrIBEsjE5tKoLlCylk34AaZjRaAKVBNcmAFtYpYYLj2f2MMoL/XV7bqe5GZoRR7aAGGgPSjTARpJ7iyg+3dj24yvrc8yArfzn9KAWQtADEEbGyXwWY3OBlpZcG2SaeHQeVnTuFb2o6RYBAshYogIFZZ/YEGm1Zm5eXKqirRYWDTfJ5Q8d3YKYJBcKEbpCp7hl9tu64+7QontUrJ/ehuvIdAZTvaytrMQxfIMxRtfEH7wt2W4oF0z65qnUGHuKl1kt2XarMNBUCMWIG3ClwOkE23j6fK7SnqN6rTPh2lDRCRjPxlbuV0GVwlUVmB3teD3L+KnHCFsNj06plrRqur6fZElc8PIOuD+8gDLAaIj73Cqk38RRvuQ0Z8sHuZo1JA1D/+/E6tzA/qdJBBghBkMzYIC1EhnVp0TRAWjbPyrgXLoxuHgPzM/FAAE7kpps2lQNrPv9w4tDsMKjBVvYJ0WyPiGi8l5GfdD12giQASgowmIljfXD/e5xU1ytJpjwJgbzHLHhgEWxOBzB2xHXwX9QvVFSFcJLhvbIhFAUwM+O6LT20pvSQa18fnqDasmGzOh1gQaP8pEQI3wTAGGIv4+2bvMPLfbQFB9StnruVaA5MgOAusBaBxFkEBFM6cz1pc7JoEIK1sYQ4CffCeqUE3C4hUxz1KYxqmRSkA6az/K0ktjekMnmJSCwLSdt8uPuwCgi1aF9AJtCmAQIGAsWhxKi9wRaoRZcaotS/q9Y/dXtiognSmaUyh25YdIn521xblIh5FsC6A0xErQgdQ1dHkkWdwUsv71yciEASAbtQmYjDx44uOYcL3+N1rX4XqDq1QANHtYjSdPb9pz01Z28FJAPHpHMsEFB2jOmx1sHAmu27AMcHK1I8Xnu9EAfcFMiE/SP/S6WvRe/++Gd0oYXEVRBwU4MXx4H6mp2IFqacGhMbZwEW4fPXb6N0Pb0Y/fGBT9J2to9fE76aTkOG9j4NbMe5DAI+EbAF+8ZMt0Y8e2FTo46OkYPX8jVKInxc8n+nfjB4WJFeJAALWIS7u++99uQh+8L2pwYb7T/4TVDxwMuiFMJ2afvj5WJmtup4mPfKo6BA++PgmcYsq6MV4hHaCDASwEiL5dXZ1wc+uu4wYwW6Rr4/1Ct2eRarSaFibwE6QWQnSAuBBF+3rNQkyy0bRvazvBNuqfT1YF9n1QjtHrPVy5+7gLABqbVQPuUnk170n01Mg4dqFfo4YuJ9agH5Iro9qlTQ9d7eJgAhU2xp1U6hZ90pmBQJAP40BghKAag/vernBtUbfP7ZHqjpELxq0bJF1vxg053psOigBrIZAfrgIqhkStTYuLAapAnNYN5OyZ5lFqaoPao1YzQqgF4IAZC0HUyK4UhMD8r+qKIVA+baJFciLBQI4SbIXlABg0mWz/6BlSM+tGnm4LjJXCFbAJBaQrSZ7voHmtgCSaBj+0JrP33bvE5uN3YCm441zXysDYm0mSDpGeHy2wJrg/IYtkSu+fltVv/2iNohNxqAEW2IFTFwY2bnEJmcSO4b/cz37Dc/5+m1VZHC9W5qsk7XpifB55w5UeSRUxTiXJwBvLYCsjQhmPldn/2wsIMsImWRy8s4l9vg84VELIFaEvYsDMBPKHqQv3dJk38MkiDU5ecYD/z/XBfLSCihXfS96IgDJ91hvzsXONyqOD4+OdwtistMWfbIAqpXh++6KSXkFx4cFsOzbt0W7EFPSuAZZFmfdArL7pYrjGwQgcqNeLYptlVR9fuDZQXGylo1lH7jtmreY5v9lFgA45ZcLJNsI7pcAZOcH38cYQMntVpGJ8BVXPeuAMGkR32dffuttjKTi9ogAEhPRiwKoDfKtGdRHEot2r2YQnO4SS4WE8fGsh2hPcHsDZBHSyeTV9lkAofTAMUmDYlHN9YVBBU7m/bKlayoICt0390cqABEpeyGCvOzIeQ+PDEJhXB7Z3/V3Rjci/3D2p8gC5EbMLgK7p7L+Pn7G73xEtufPoIWjaKtOyLmsjJAOdi9/kbxt5/gRDqOfzP6PyP5YFCGd4PgRPga/ugLocvwIx9EdWwAicKAICGfJLwt+dS0A8CLHkXAUhdwtFIBPKVEiKCwXzf66FoDBMOEitDirJQCxhWyFY0o4ghXdps8mtbJHOK6EI9DmqrYARCVdl2NLNBzdvKpPGxZAK6omiJphxFEjAYiomiIgGkt+nczPJBYAOB553keUcBJrgptRqQJIFLbGgJhoYuAruGmEsVsG8IR5okFA2nN+nH+cpGXAIsedaAjG5uLYAmBATLga+FpxgTKu0NnI8w30RGOBTg87J7mAja5JixGzQkT1WLPhhk8sALHqRleIqMP1mbh/lbXGkYkr9HrytsDnQlQAlDo/Y+NCNhtHwhz1+WyIktGPLGYgrbYOTqwAguGzfEZEidhpw/UpwwKk8QBXiYmycMQm+a1bgIwlWEreOnxehEWgzNn64mtppydwfYCwiInz/ZW4QEOYZ1BMWAp658u6eGkCEJV5SFVxkYwYFwMOjVPlWbsLlHGF4AahcpQ9RglT8s/bDnqrdIFSS4AvwMpRwhSLZZO/EgEIESxTBIQh+StpxlbpGZqJO9RJ3pb4fIkC8ner+rDKD5GlCIimkL8WAVAERFPIX5sAhAgWhAiYHQoba1X6/I0RgBABU6Qk/3wV2R4ZWnV+e/HFuWIcJvp1k792C5CxBNuFJWDtUBjoCfLXXiUQN2lUWEUaBEqp6nTSBcpxiTAw3E/gL440ifyNswBDwTH2GM+QM974+8/U7e833gIMBceo/+bZZO4Dz3BnE8nfWAswZA0OJ28vREyVugYEuGhdcrzJNxm7MJLCJUKAzCyRGxhUADd11ndOABkhHBXWgGguMOsfdeVmY9dGNxHBjLAGc+Rao7AiZv2+SzcduzraoqDuGGODRvj6R+ooZAtaAEIEIP9hukX1uTvJ63gTVnSDFMCQWwQRdMjJStCNJuzLTwGUI4S2cIsYH5Tn5x9xIbsTpAAyQoAADkXsVm0LWMw6kRB/xbcvFvv81Oga0dUJWgBDQoAIDkSsLyoCyH4yWq/a7Pv+ZePQnq5In+6ne5Tr5pxyNZ1JAYxnFRaEVQi1xKInZvvlEGZ7CkAuhrYQw/4AxADSnxKk74X+7CkAuWXYE62nU11facYi1UryWg15pqcAxhfEnBDCrCOCSAl/Du8+pi4pgPotRFu8YCVmovoyS33xWhWuTY8zPAVQp6WIotur0Hsyf54b87LZ2Xs1+zvO7HbwPwEGAD3uJdpuNu96AAAAAElFTkSuQmCC");
        } else {
            mxCellElement.addAttribute("style", "rounded=1;strokeColor=#3366FF;fillColor=#F8F8FF;strokeWidth=3;");
        }
        mxCellElement.addAttribute("parent", "1");
        mxCellElement.addAttribute("vertex", "1");
        Element mxGeometryElement = new DefaultElement("mxGeometry");
        if (taskType.equals("batchtask")) {
            mxGeometryElement.addAttribute("y", String.valueOf(Double.valueOf(y)));
        } else {
            mxGeometryElement.addAttribute("y", String.valueOf(Double.valueOf(y)));
        }
        mxGeometryElement.addAttribute("x", String.valueOf(Double.valueOf(x) + 130));
        if (taskType.equals("batchtask")) {
            mxGeometryElement.addAttribute("width", "48");
            mxGeometryElement.addAttribute("height", "48");
        } else {
            mxGeometryElement.addAttribute("width", "80");
            mxGeometryElement.addAttribute("height", "40");
        }
        mxGeometryElement.addAttribute("as", "geometry");
        mxCellElement.add(mxGeometryElement);
        taskElement.add(mxCellElement);
        rootMxCellElement.add(taskElement);
        //改之前线的source ID
        updateLineSource(rootMxCellElement, toTaskId, addTaskNodeId, toTargetId);
        //添加线
        createLineNode(rootMxCellElement, toTaskId, addTaskNodeId);
        //所有节点往后移
        coordinateMovement(rootMxCellElement, toTaskId, addTaskNodeId);
    }


    private static void coordinateMovement(Element rootMxCellElement, String toTaskId, String newNodeId) {
        Element toNode = null;
        List<Element> tasks = new ArrayList<>();
        List<Element> lines = new ArrayList<>();
        for (Element cell : rootMxCellElement.elements()) {
            if (!Strings.isNullOrEmpty(cell.attributeValue("type"))) {
                tasks.add(cell);
                if (cell.attributeValue("id").equals(toTaskId)) {
                    toNode = cell;
                }
            }
            if (!Strings.isNullOrEmpty(cell.attributeValue("nodeType")) && cell.attributeValue("nodeType").equals("line")) {
                Element element = cell.element("mxGeometry").element("Array");
                if (element == null) {
                    continue;
                }
                lines.add(cell);
                lines.add(cell);
            }
        }

        double toNodeX = getElementX(toNode);
        double toNodeY = getElementY(toNode);
        // 设定统一移动的距离
        int moveDistance = 160;  // 例如：统一向右移动 100 个单位
        // 遍历所有节点，修改x轴坐标
        List<String> moveTasks = new ArrayList<>();
        for (Element task : tasks) {
            if (newNodeId.equals(task.attributeValue("id"))) {
                moveTasks.add(task.attributeValue("id"));
                continue;
            }
            double taskNodeX = getElementX(task);
            double taskNodeY = getElementY(task);
            double difference = toNodeY - taskNodeY;
            // 如果节点在新节点的右边（x 大于新节点的 x）且 y 坐标一致
            if (taskNodeX - toNodeX > 20) {
                // 统一向右移动
                moveTasks.add(task.attributeValue("id"));
//                System.out.println(String.format("当前节点%s,当前位置%s,移动位置%s", task.attributeValue("name"), taskNodeX, taskNodeX + moveDistance));
                setElementX(task, taskNodeX + moveDistance);
            }
        }

        //遍历所有线，修改x轴坐标
        for (Element line : lines) {
            if (moveTasks.contains(line.attributeValue("source"))) {
                setLineX(line, getLineX(line) + 80);
            }
        }
    }

    private static void updateLineSource(Element rootMxCellElement, String sourceId, String updateSourceId, String targetId) {
        List<Element> list = new ArrayList<>();
        for (Element cell : rootMxCellElement.elements()) {
            if (!Strings.isNullOrEmpty(cell.attributeValue("source")) && cell.attributeValue("source").equals(sourceId)) {
                if (Strings.isNullOrEmpty(targetId)) {
                    list.add(cell);
                } else if (!Strings.isNullOrEmpty(targetId) && cell.attributeValue("target").equals(targetId)) {
                    list.add(cell);
                }
            }
        }

        if (list.size() != 1) {
            throw new PlatformException("加签查询到多条出线，请选择其中一条！", PlatformExceptionEnum.UNKOWN_ERROR);
        }

        if (list.size() == 1) {
            Element element = list.get(0);
            element.addAttribute("source", updateSourceId);
        }

    }

    private static Element getElementById(Element rootMxCellElement, String id) {
        for (Element cell : rootMxCellElement.elements()) {
            if (cell.attributeValue("id").equals(id)) {
                Element mxGeometryInfo = cell.element("mxCell").element("mxGeometry");
                return mxGeometryInfo;
            }
        }
        return null;
    }


    private static double getElementX(Element element) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        return Double.valueOf(mxGeometryInfo.attributeValue("x", "0"));
    }


    private static double getElementY(Element element) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        return Double.valueOf(mxGeometryInfo.attributeValue("y", "0"));
    }


    private static void setElementX(Element element, double x) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        mxGeometryInfo.addAttribute("x", String.valueOf(x));
    }


    private static void setElementY(Element element, double y) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        mxGeometryInfo.addAttribute("y", String.valueOf(y));
    }

    private static double getLineX(Element element) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        return Double.valueOf(mxGeometryInfo.attributeValue("x", "0"));
    }


    private static double getLineY(Element element) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        return Double.valueOf(mxGeometryInfo.attributeValue("y", "0"));
    }


    private static void setLineX(Element element, double x) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        mxGeometryInfo.addAttribute("x", String.valueOf(x));
    }


    private static void setLineY(Element element, double y) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        mxGeometryInfo.addAttribute("y", String.valueOf(y));
    }

    private static Map<String, JSONObject> getChildShapesByJson(String json) {
        Map<String, JSONObject> result = new HashMap<>();
        JSONArray childShapes = JSONObject.parseObject(json).getJSONArray("childShapes");
        for (Object o : childShapes) {
            String resourceId = ((JSONObject) o).getString("resourceId");
            result.put(resourceId, (JSONObject) o);
        }
        return result;
    }

    private static String setChildShapes(String json, Map<String, JSONObject> childShapes) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONArray jsonArray = new JSONArray();
        for (String key : childShapes.keySet()) {
            jsonArray.add(childShapes.get(key));
        }
        jsonObject.put("childShapes", jsonArray);
        return jsonObject.toString();
    }

    public static String updateJsonByXml(String json, Map<String, AbstractElementInfoVo> allElement, List<AdjustSerialUserInfoVo> users) {
        //获取json中所有的node和line，对比map中的node和line
        Map<String, JSONObject> childShapes = getChildShapesByJson(json);


        for (String key : allElement.keySet()) {
            //找不到就增加，找到就修改
            JSONObject jsonElement = childShapes.get(key);
            AbstractElementInfoVo elementInfo = allElement.get(key);
            if (jsonElement != null) {//修改
                //如果是节点,目前可以修改的只有位置
                if (elementInfo.getType().equals(NodeInfoVo.TYPE)) {
                    NodeInfoVo nodeInfo = (NodeInfoVo) elementInfo;
                    jsonElement.put("x", nodeInfo.getX());
                    jsonElement.put("y", nodeInfo.getY());
                    JSONArray target = jsonElement.getJSONArray("target");
                    target.clear();
                    if (nodeInfo.getTargets() != null && nodeInfo.getTargets().size() > 0) {
                        for (String tar : nodeInfo.getTargets()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("resourceId", tar);
                            target.add(jsonObject);
                        }
                    }
                }
                //如果是线
                if (elementInfo.getType().equals(LineInfoVo.TYPE)) {
                    LineInfoVo lineInfo = (LineInfoVo) elementInfo;
                    JSONArray target = jsonElement.getJSONArray("target");
                    if (target != null) {
                        target.getJSONObject(0).put("resourceId", lineInfo.getTarget());
                    }

                    JSONArray source = jsonElement.getJSONArray("source");
                    if (source != null) {
                        source.getJSONObject(0).put("resourceId", lineInfo.getSource());
                    }
                }
            } else {//增加

                if (elementInfo.getType().equals(LineInfoVo.TYPE)) {
                    LineInfoVo lineInfo = (LineInfoVo) elementInfo;
                    JSONObject nodeJson = LineInfoVo.getLineJson(lineInfo.getId(), lineInfo.getTarget(), lineInfo.getSource());
                    childShapes.put(nodeJson.getString("resourceId"), nodeJson);
                }

                if (elementInfo.getType().equals(NodeInfoVo.TYPE)) {
                    NodeInfoVo nodeInfo = (NodeInfoVo) elementInfo;
                    AdjustSerialUserInfoVo addSerialUserInfo = null;
                    for (AdjustSerialUserInfoVo a : users) {
                        if (a.getNodeId().equals(nodeInfo.getId())) {
                            addSerialUserInfo = a;
                            break;
                        }
                    }
                    JSONObject nodeJson = NodeInfoVo.getTaskUserJson(nodeInfo.getId(), nodeInfo.getX(), nodeInfo.getY(), nodeInfo.getTargets(), addSerialUserInfo);
                    childShapes.put(nodeJson.getString("resourceId"), nodeJson);
                }

            }
        }
        return setChildShapes(json, childShapes);
    }

    public static String getUuid() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 15);
        return uuid;
    }

    public static Map<String, AbstractElementInfoVo> getAllNodeAndLine(String xml) {
        Map<String, AbstractElementInfoVo> result = new HashMap<>();

        Map<String, LineInfoVo> resultLine = new HashMap<>();
        Map<String, NodeInfoVo> resultNode = new HashMap<>();

        InputSource in = new InputSource(new StringReader(xml));
        // 创建 SAXReader 对象
        SAXReader reader = new SAXReader();
        // 读取 XML 文件
        org.dom4j.Document document = null;
        try {
            document = reader.read(in);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        // 获取根元素
        Element root = document.getRootElement().element("root");
        for (Element cell : root.elements()) {
            if (cell.attributeValue("id").equals("0") || cell.attributeValue("id").equals("1")) {
                continue;
            }
            if (!Strings.isNullOrEmpty(cell.attributeValue("nodeType"))) {
                LineInfoVo lineInfo = buildElementToLine(cell);
                resultLine.put(lineInfo.getId(), lineInfo);
            } else {
                NodeInfoVo nodeInfo = buildElementToTask(cell);
                resultNode.put(nodeInfo.getId(), nodeInfo);
            }
        }

        for (String nodeKey : resultNode.keySet()) {
            NodeInfoVo nodeInfo = resultNode.get(nodeKey);
            for (String key : resultLine.keySet()) {
                LineInfoVo lineInfo = resultLine.get(key);
                if (lineInfo.getSource().equals(nodeInfo.getId())) {
                    if (nodeInfo.getTargets() == null) {
                        nodeInfo.setTargets(new ArrayList<>());
                    }
                    nodeInfo.getTargets().add(lineInfo.getId());
                }
            }
        }

        result.putAll(resultNode);
        result.putAll(resultLine);
        return result;
    }

    private static LineInfoVo buildElementToLine(Element element) {
        LineInfoVo lineInfo = new LineInfoVo();
        String id = element.attributeValue("id");
        String source = element.attributeValue("source");
        String target = element.attributeValue("target");
        lineInfo.setId(id);
        lineInfo.setSource(source);
        lineInfo.setTarget(target);
        return lineInfo;
    }

    private static NodeInfoVo buildElementToTask(Element element) {
        NodeInfoVo nodeInfo = new NodeInfoVo();
        String id = element.attributeValue("id");
        String name = element.attributeValue("label");
        Element mxGeometry = element.element("mxCell").element("mxGeometry");
        String x = mxGeometry.attributeValue("x", "0");
        String y = mxGeometry.attributeValue("y", "0");
        String width = mxGeometry.attributeValue("width");
        String height = mxGeometry.attributeValue("height");
        nodeInfo.setId(id);
        nodeInfo.setName(name);
        nodeInfo.setX(x);
        nodeInfo.setY(y);
        nodeInfo.setWidth(width);
        nodeInfo.setHeight(height);
        return nodeInfo;
    }

}
