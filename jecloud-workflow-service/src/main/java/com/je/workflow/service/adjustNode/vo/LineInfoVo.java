package com.je.workflow.service.adjustNode.vo;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

/**
 * xml转成line对象使用
 */
public class LineInfoVo extends AbstractElementInfoVo {

    public static final String TYPE = "line";

    /**
     * 连接节点id
     */
    private String target;
    /**
     * 出线节点id
     */
    private String source;

    public LineInfoVo() {
        super(TYPE);
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 创建线json对象
     *
     * @param id       id
     * @param targetId 抵达节点id
     * @param source   来源节点id
     * @return
     */
    public static JSONObject getLineJson(String id, String targetId, String source) {
        JSONObject lineJson = new JSONObject();
        lineJson.put("child", new JSONArray());
        lineJson.put("bounds", new JSONObject());
        lineJson.put("resourceId", id);
        lineJson.put("childShapes", new JSONArray());
        JSONObject stencil = new JSONObject();
        stencil.put("id", "line");
        lineJson.put("stencil", stencil);
        JSONArray targetArray = new JSONArray();
        JSONObject targetResourceId = new JSONObject();
        targetResourceId.put("resourceId", targetId);
        targetArray.add(targetResourceId);
        lineJson.put("target", targetArray);

        JSONObject resourceId = new JSONObject();
        resourceId.put("resourceId", id);
        resourceId.put("name", "");
        resourceId.put("conditionExpression", "");
        lineJson.put("properties", resourceId);

        JSONObject styleConfig = new JSONObject();
        styleConfig.put("dashed", "0");
        styleConfig.put("strokeColor", "");
        styleConfig.put("fontSize", "12");
        styleConfig.put("fontFamily", "宋体");
        styleConfig.put("fontColor", "");
        lineJson.put("styleConfig", styleConfig);

        JSONArray sourceArray = new JSONArray();
        JSONObject sourceResourceId = new JSONObject();
        sourceResourceId.put("resourceId", source);
        sourceArray.add(sourceResourceId);
        lineJson.put("source", sourceArray);
        return lineJson;

    }

}
