/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.runtime.shared.identity.UserServeCustomizeManager;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.servicecomb.RpcSchemaFactory;
import com.je.workflow.model.WorkFlowInfoVo;
import com.je.workflow.rpc.dictionary.WorkFlowCustomBeanInvokeRpcService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServeCustomizeManagerImpl implements UserServeCustomizeManager {

    @Override
    public Object findUsers(String service, String method, KaiteBaseUserTask kaiteBaseUserTask, String prod,
                            Map<String, Object> bean, Boolean multiple, Boolean addOwn, String operationId, String starterUser) {
        WorkFlowCustomBeanInvokeRpcService customBeanInvokeRpcService;
        customBeanInvokeRpcService = RpcSchemaFactory.getRemoteProvierClazz(prod, "workFlowCustomBeanInvokeRpcService",
                WorkFlowCustomBeanInvokeRpcService.class);
        WorkFlowInfoVo workFlowInfoVo = new WorkFlowInfoVo();
        workFlowInfoVo.setMultiple(multiple);
        workFlowInfoVo.setBean(bean);
        workFlowInfoVo.setNodeId(kaiteBaseUserTask.getId());
        workFlowInfoVo.setNodeName(kaiteBaseUserTask.getName());
        workFlowInfoVo.setOperationId(WorkFlowInfoVo.ExecutionTypeEnum.getType(operationId));
        workFlowInfoVo.setStarterUser(starterUser);
        return customBeanInvokeRpcService.invoke(service, method, workFlowInfoVo, multiple, addOwn);
    }

    @Override
    public Boolean checkContainsCurrentUser(String service, String method, KaiteBaseUserTask kaiteBaseUserTask,
                                            String userId, String prod, Map<String, Object> bean, String operationId, String starterUser) {
        Object users = findUsers(service, method, kaiteBaseUserTask, prod, bean, true, true, operationId, starterUser);
        if (users != null && users instanceof JSONTreeNode) {

            return checkJsonTreeNodeContainsCurrentUser((JSONTreeNode) users, userId);
        }
        return false;
    }

    private Boolean checkJsonTreeNodeContainsCurrentUser(JSONTreeNode jsonTreeNode, String deptmentUserId) {
        if (jsonTreeNode.getId() != null && jsonTreeNode.getId().equals(deptmentUserId)) {
            return true;
        }
        List<JSONTreeNode> children = jsonTreeNode.getChildren();
        if (children != null && children.size() > 0) {
            for (JSONTreeNode childrenNode : children) {
                return checkJsonTreeNodeContainsCurrentUser(childrenNode, deptmentUserId);
            }
        }
        return false;
    }

}
