package com.je.workflow.service.adjustNode;

import com.je.workflow.service.adjustNode.vo.DelNodeParamsVo;

import java.io.File;
import java.util.HashMap;

public interface DelNodeService {

    File delNode(DelNodeParamsVo vo);

    HashMap<String, String> getXmlAndJson(String beanId, String pdid);
}
