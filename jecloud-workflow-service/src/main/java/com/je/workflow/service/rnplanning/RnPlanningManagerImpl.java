/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.workflow.service.rnplanning;

import com.google.common.base.Strings;
import com.je.bpm.engine.rnplanning.RnPlanningManager;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RnPlanningManagerImpl implements RnPlanningManager {

    @Autowired
    private MetaService metaService;

    @Override
    public String getProcessDefinitionIdByProcessInstanceId(String processInstanceId, String beanId) {

        DynaBean csBeanInfo = null;
        if (!Strings.isNullOrEmpty(beanId)) {
            csBeanInfo = metaService.selectOne("JE_WORKFLOW_CS_PROCESSINFO", ConditionsWrapper.builder().eq("BUSINESS_KEY_", beanId));
            if (csBeanInfo != null) {
                return csBeanInfo.getStr("PROC_DEF_ID_");
            }
        }

        if (Strings.isNullOrEmpty(processInstanceId)) {
            return null;
        }

        DynaBean dynaBean = metaService.selectOne("JE_WORKFLOW_RN_PLANNING", ConditionsWrapper.builder().eq("PROCESS_INSTANCE_ID_", processInstanceId));
        if (dynaBean == null) {
            return null;
        }
        String key = dynaBean.getStr("PLANNING_NEW_MODEL_KEY");
        DynaBean prodef = metaService.selectOne("act_re_procdef", ConditionsWrapper.builder().eq("KEY_", key));
        if (prodef == null) {
            return null;
        } else {
            return prodef.getStr("ID_");
        }
    }
}
