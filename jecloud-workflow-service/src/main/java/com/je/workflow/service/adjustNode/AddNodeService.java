package com.je.workflow.service.adjustNode;

import com.je.workflow.service.adjustNode.vo.AddNodeParamsVo;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface AddNodeService {
    /**
     * 添加串行节点
     *
     * @return
     */
    File addSerialNode(AddNodeParamsVo vo);

    /**
     * 获取加签节点
     *
     * @param beanId
     * @param taskId
     * @param piid
     * @return
     */
    List<Map<String, String>> getAddNodes(String beanId, String taskId, String piid);

}
