package com.je.workflow.service.handover;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.config.task.assignment.AssigneeResource;
import com.je.bpm.core.model.config.task.assignment.BasicAssignmentConfigImpl;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;
import com.je.bpm.core.model.config.task.assignment.UserAssignmentConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.engine.task.GetTakeNodeNameUtil;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.workflow.service.user.WorkFlowUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class HandoverServiceImpl implements HandoverService {

    @Autowired
    private ProcessEngine processEngine;
    @Autowired
    private MetaService metaService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private WorkFlowUserService workFlowUserService;

    @Override
    public Page load(String userId) {
        Page page = new Page(0, 1);
        List<Map<String, Object>> records = new ArrayList<>();
        if (Strings.isNullOrEmpty(userId)) {
            page.setRecords(new ArrayList());
            page.setTotal(0);
            return page;
        }
        String sql = "SELECT PROC_DEF_ID_ FROM ACT_RU_EXECUTION GROUP BY PROC_DEF_ID_";
        List<Map<String, Object>> list = metaService.selectSql(sql);

        Set<String> pdids = new HashSet<>();

        for (Map<String, Object> map : list) {
            pdids.add((String) map.get("PROC_DEF_ID_"));
        }

        List<ProcessDefinition> processDefinitions = processEngine.getRepositoryService().createProcessDefinitionQuery().latestVersion().list();

        for (ProcessDefinition processDefinition : processDefinitions) {
            pdids.add(processDefinition.getId());
        }

        for (String pdid : pdids) {
            BpmnModel bpmnModel = processEngine.getRepositoryService().getBpmnModel(pdid);
            if (bpmnModel == null) {
                continue;
            }
            Map<String, FlowElement> flowElementMap = bpmnModel.getMainProcess().getFlowElementMap();
            DynaBean needResultBean = null;

            //遍历所有节点
            for (String key : flowElementMap.keySet()) {
                FlowElement flowElement = flowElementMap.get(key);
                if (!(flowElement instanceof KaiteBaseUserTask)) {
                    continue;
                }
                KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
                boolean exist = false;
                //拿到人员配置
                TaskAssigneeConfigImpl taskAssigneeConfig = kaiteBaseUserTask.getTaskAssigneeConfig();
                List<BasicAssignmentConfigImpl> basicAssignmentConfigList = taskAssigneeConfig.getResource();
                for (BasicAssignmentConfigImpl basicAssignmentConfig : basicAssignmentConfigList) {
                    if (exist) {
                        break;
                    }
                    //只获取按人员处理配置
                    if (!(basicAssignmentConfig instanceof UserAssignmentConfigImpl)) {
                        continue;
                    }
                    UserAssignmentConfigImpl userAssignmentConfig = (UserAssignmentConfigImpl) basicAssignmentConfig;
                    List<AssigneeResource> assigneeResourceList = userAssignmentConfig.getResource();
                    for (AssigneeResource assigneeResource : assigneeResourceList) {
                        if (assigneeResource.getId().indexOf(userId) >= 0) {
                            exist = true;
                            break;
                        }
                    }
                }
                if (exist) {
                    if (needResultBean != null) {
                        if (Strings.isNullOrEmpty(kaiteBaseUserTask.getName())) {
                            needResultBean.setStr("EXECUTION_NODE_NAME", needResultBean.getStr("EXECUTION_NODE_NAME")
                                    + "," + GetTakeNodeNameUtil.builder().getTakeNodeName(kaiteBaseUserTask));
                        } else {
                            needResultBean.setStr("EXECUTION_NODE_NAME", needResultBean.getStr("EXECUTION_NODE_NAME")
                                    + "," + kaiteBaseUserTask.getName());
                        }
                    } else {
                        needResultBean = new DynaBean();
                        needResultBean.setStr("PROCESS_NAME", bpmnModel.getMainProcess().getName());
                        needResultBean.setStr("FUNC_CODE", bpmnModel.getMainProcess().getProcessConfig().getFuncCode());
                        needResultBean.setStr("FUNC_NAME", bpmnModel.getMainProcess().getProcessConfig().getFuncName());
                        if (Strings.isNullOrEmpty(kaiteBaseUserTask.getName())) {
                            needResultBean.setStr("EXECUTION_NODE_NAME", GetTakeNodeNameUtil.builder().getTakeNodeName(kaiteBaseUserTask));
                        } else {
                            needResultBean.setStr("EXECUTION_NODE_NAME", kaiteBaseUserTask.getName());
                        }
                        needResultBean.setStr("ID_", pdid);
                    }
                }
            }

            if (needResultBean != null) {
                records.add(needResultBean.getValues());
            }

        }
        page.setTotal(records.size());
        page.setRecords(records);
        return page;
    }


    @Override
    @Transactional
    public void processAllHandover(String assignee, String toAssignee) {
        //交接全部
        Page page = load(assignee);
        List<Map<String, Object>> records = page.getRecords();
        List<String> ids = new ArrayList<>();
        for (Map<String, Object> map : records) {
            ids.add((String) map.get("ID_"));
        }
        processHandoverByIds(assignee, toAssignee, String.join(",", ids));
    }

    @Override
    @Transactional
    public void processHandoverByIds(String assignee, String toAssignee, String ids) {
        for (String pdid : ids.split(",")) {
            //交接指定的流程
            String[] pdidArray = pdid.split(":");
            String key = pdidArray[0];
            String version = pdidArray[1];
            String modelId = "";
            String xmlId = "";
            DynaBean dynaBean = metaService.selectOne("JE_WORKFLOW_PROCESSINFO", ConditionsWrapper.builder()
                    .eq("PROCESSINFO_KEY", key));
            if (dynaBean != null) {//修改流程信息表
                String processinfo_version = dynaBean.getStr("PROCESSINFO_VERSION");
                if (processinfo_version.equals(version)) {
                    modelId = dynaBean.getStr("PROCESSINFO_MODEL_ID");
                    xmlId = dynaBean.getStr("PROCESSINFO_RESOURCE_XML_ID");
                }
            } else {//修改单独改的流程
                dynaBean = metaService.selectOne("JE_WORKFLOW_RN_PLANNING", ConditionsWrapper.builder()
                        .eq("PLANNING_NEW_MODEL_KEY", key));
                modelId = dynaBean.getStr("PLANNING_MODEL_ID");
                xmlId = dynaBean.getStr("RESOURCE_XML_ID");
            }


            if (!Strings.isNullOrEmpty(modelId)) {//修改json
                DynaBean model = metaService.selectOne("act_re_model", ConditionsWrapper.builder().eq("ID_", modelId));
                String resourceId = model.getStr("EDITOR_SOURCE_VALUE_ID_");
                DynaBean metaInfoJson = metaService.selectOne("act_ge_bytearray", ConditionsWrapper.builder().eq("ID_", resourceId));
                updateRnPlanningResourceInfo(metaInfoJson, assignee, toAssignee);
            }

            if (!Strings.isNullOrEmpty(xmlId)) {//修改xml
                DynaBean metaInfoXMl = metaService.selectOne("act_ge_bytearray", ConditionsWrapper.builder().eq("ID_", xmlId));
                updateRnPlanningResourceInfo(metaInfoXMl, assignee, toAssignee);
            }

            //修改部署xml信息
            String deploymentId = metaService.selectOneByPk("act_re_procdef", pdid).getStr("DEPLOYMENT_ID_");
            DynaBean deploymentBytearray = metaService.selectOne("act_ge_bytearray",
                    ConditionsWrapper.builder().eq("DEPLOYMENT_ID_", deploymentId));
            updateRnPlanningResourceInfo(deploymentBytearray, assignee, toAssignee);

            runtimeService.removeRemoveProcessDefinitionInfo(pdid);
        }
    }


    private void updateRnPlanningResourceInfo(DynaBean dynaBean, String assignee, String toAssignee) {
        Object bytes = dynaBean.get("BYTES_");
        String resource = new String((byte[]) bytes);
        resource = resource.replaceAll(assignee, toAssignee).replaceAll(workFlowUserService.getUserNameById(assignee)
                , workFlowUserService.getUserNameById(toAssignee)).
                replaceAll(workFlowUserService.getUserCodeById(assignee), workFlowUserService.getUserCodeById(toAssignee));
        dynaBean.set("BYTES_", resource.getBytes());
        metaService.update(dynaBean);
    }

}
