package com.je.workflow.service.adjustNode.vo;

public class AbstractElementInfoVo {

    public AbstractElementInfoVo(String type) {
        this.type = type;
    }

    /**
     * 元素id
     */
    protected String id;
    /**
     * 元素类型 只有两种，line 线 和 node 节点
     */
    protected String type;
    /**
     * x轴
     */
    private String x;
    /**
     * y轴
     */
    private String y;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }
}
