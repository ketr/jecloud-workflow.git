package com.je.workflow.service.adjustNode.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.json.converter.json.converter.BpmnJsonConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.model.process.ModelOperatorService;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.workflow.service.adjustNode.vo.AbstractElementInfoVo;
import com.je.workflow.service.adjustNode.vo.LineInfoVo;
import com.je.workflow.service.adjustNode.vo.NodeInfoVo;
import com.je.workflow.service.model.ModelService;
import com.je.workflow.service.model.ModelServiceImpl;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.Blob;
import java.util.*;

/**
 * 调整节点父类
 */
public class AbstractAdjustNodeService {

    @Autowired
    protected MetaService metaService;
    @Autowired
    protected ModelService modelService;
    @Autowired
    protected ModelOperatorService modelOperatorService;
    @Autowired
    protected RepositoryService repositoryService;
    @Autowired
    protected RuntimeService runtimeService;


    public File updateCsInfo(String updateJson, String xml, String pdid, String beanId, String piid) {
        //把xml和json信息保存到加签规划表中
        //部署，获取新的pdid
        //先查看是否部署过，如果部署过，直接修改xml，没有部署过，在创建model进行部署
        DynaBean csDynaBean = metaService.selectOne("JE_WORKFLOW_CS_PROCESSINFO",
                ConditionsWrapper.builder().eq("BUSINESS_KEY_", beanId));
        String newPdid = "";
        if (csDynaBean != null) {
            newPdid = csDynaBean.getStr("PROC_DEF_ID_");
            String[] pdidArray = pdid.split(":");
            String key = pdidArray[0];
            String[] newKeyArrray = newPdid.split(":");
            String newKey = newKeyArrray[0];
            updateJson = updateJson.replaceAll(key, newKey);
            xml = xml.replaceAll(key, newKey);
        }

        newPdid = modelDeploy(pdid, updateJson, beanId, newPdid);
        boolean isInsert = false;
        if (csDynaBean == null) {
            isInsert = true;
            csDynaBean = new DynaBean("JE_WORKFLOW_CS_PROCESSINFO", true);
            csDynaBean.setStr("BUSINESS_KEY_", beanId);
            csDynaBean.setStr("PROC_HI_DEF_ID_", pdid);
            csDynaBean.setStr("PROC_INST_ID_", piid);
        } else {
            metaService.delete(ConditionsWrapper.builder().table("JE_WORKFLOW_CS_BYTEARRY").eq("JE_WORKFLOW_CS_PROCESSINFO_ID", csDynaBean.getPkValue()));
        }
        csDynaBean.setStr("PROC_DEF_ID_", newPdid);

        if (isInsert) {
            metaService.insert(csDynaBean);
        } else {
            metaService.update(csDynaBean);
        }

        //添加xml规划信息
        DynaBean csDynaBeanXml = new DynaBean("JE_WORKFLOW_CS_BYTEARRY", true);
        csDynaBeanXml.setStr("JE_WORKFLOW_CS_PROCESSINFO_ID", csDynaBean.getPkValue());
        csDynaBeanXml.setStr("BYTEARRY_TYPE_", "xml");
        csDynaBeanXml.set("BYTES_", xml.getBytes());
        metaService.insert(csDynaBeanXml);

        //添加json规划信息
        DynaBean csDynaBeanJson = new DynaBean("JE_WORKFLOW_CS_BYTEARRY", true);
        csDynaBeanJson.setStr("JE_WORKFLOW_CS_PROCESSINFO_ID", csDynaBean.getPkValue());
        csDynaBeanJson.setStr("BYTEARRY_TYPE_", "json");
        csDynaBeanJson.set("BYTES_", updateJson.getBytes());
        metaService.insert(csDynaBeanJson);
        return modelService.getModelSvgByXml(xml);
    }


    public String modelDeploy(String oldPdid, String json, String beanId, String newPdid) {
        String[] pdidArray = oldPdid.split(":");
        String key = pdidArray[0];
        DynaBean processInfo = metaService.selectOne("JE_WORKFLOW_PROCESSINFO", ConditionsWrapper.builder().eq("PROCESSINFO_KEY", key));
        String funcCode = processInfo.getStr("PROCESSINFO_FUNC_CODE");
        newPdid = saveWorkflowRnPlanning(json, funcCode, key, beanId, newPdid);
        return newPdid;
    }

    private String saveWorkflowRnPlanning(String metaInfoJson, String funcCode, String oldKey, String beanId, String newPdid) {
        DynaBean dynaBean = metaService.selectOne("JE_WORKFLOW_CS_PROCESSINFO", ConditionsWrapper.builder().eq("BUSINESS_KEY_", beanId));
        boolean isExist = true;
        if (dynaBean == null) {
            isExist = false;
        }
        if (!isExist) {
            //创建model，设置xml信息
            // json里面的id需要修改 model发布的时候，不能和之前的key重复
            String newModelKey = ModelServiceImpl.generateUniqueString();
            if (metaInfoJson.indexOf(oldKey) >= 0) {
                metaInfoJson = metaInfoJson.replaceAll(oldKey, newModelKey);
            }
            //cs 表示加签过的流程model
            String modelId = modelOperatorService.saveModel("", metaInfoJson, funcCode, "cs").getEntity().getId();
            modelOperatorService.modelDeploy(modelId);
            List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().
                    processDefinitionKey(newModelKey).latestVersion().setNotCs(false).list();
            newPdid = list.get(0).getId();
        } else {
            String[] pdidArray = newPdid.split(":");
            String key = pdidArray[0];
            //修改已经部署的model xml信息，并清理缓存
            DynaBean deploy = metaService.selectOne("act_re_deployment", ConditionsWrapper.builder()
                    .eq("KEY_", key));
            DynaBean byteBean = metaService.selectOne("act_ge_cs_bytearray", ConditionsWrapper.builder()
                    .eq("DEPLOYMENT_ID_", deploy.getStr("ID_")));
            //修改真正运行的xml信息
            metaInfoJson = metaInfoJson.replaceAll(oldKey, key);
            updateRnPlanningResourceInfo(byteBean.getPkValue(), jsonToXml(metaInfoJson));
            DynaBean procdef = metaService.selectOne("act_re_procdef", ConditionsWrapper.builder().eq("KEY_", key));
            runtimeService.removeRemoveProcessDefinitionInfo(procdef.getStr("ID_"));
        }
        return newPdid;
    }

    private String jsonToXml(String json) {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = null;
        try {
            modelNode = mapper.readTree(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        BpmnModel bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes = new String(convertToXML);
        return bytes;
    }

    private void updateRnPlanningResourceInfo(String byteArrayId, String resourceInfo) {
        DynaBean metaInfoXMl = metaService.selectOne("act_ge_cs_bytearray", ConditionsWrapper.builder().eq("ID_", byteArrayId));
        metaInfoXMl.set("BYTES_", resourceInfo.getBytes());
        metaService.update(metaInfoXMl);
    }

    public HashMap<String, String> getXmlAndJson(String beanId, String pdid) {
        HashMap<String, String> result = new HashMap<>();
        //先看有没有加签过
        DynaBean csBean = metaService.selectOne("JE_WORKFLOW_CS_PROCESSINFO", ConditionsWrapper.builder().eq("BUSINESS_KEY_", beanId));
        if (csBean != null) {
            List<DynaBean> list = metaService.select("JE_WORKFLOW_CS_BYTEARRY", ConditionsWrapper.builder().eq("JE_WORKFLOW_CS_PROCESSINFO_ID", csBean.getPkValue()));
            DynaBean xmlbean = new DynaBean();
            DynaBean jsonBean = new DynaBean();
            for (DynaBean dynaBean : list) {
                if (dynaBean.getStr("BYTEARRY_TYPE_").equals("xml")) {
                    xmlbean = dynaBean;
                } else if (dynaBean.getStr("BYTEARRY_TYPE_").equals("json")) {
                    jsonBean = dynaBean;
                }
            }
            result.put("metaInfoXml", getXmlInfoById(xmlbean, "BYTES_"));
            result.put("metaInfo", getXmlInfoById(jsonBean, "BYTES_"));
            return result;
        }

        //再根据运行时的pdid查询，有两种情况，一种是已经启动了，需要查询历史规划信息
        DynaBean dynaBean = metaService.selectOne("ACT_HI_BYTEARRAY", ConditionsWrapper.builder().eq("PROC_DEF_ID_", pdid));
        if (dynaBean != null) {
            result.put("metaInfoXml", getXmlInfoById(dynaBean, "XML_BYTES_"));
            result.put("metaInfo", getXmlInfoById(dynaBean, "JSON_BYTES_"));
        } else {//二种需要查询最新的规划信息
            HashMap<String, Object> map = modelService.getInfoByPiid("", pdid);
            result.put("metaInfoXml", (String) map.get("metaInfoXml"));
            result.put("metaInfo", (String) map.get("metaInfo"));
        }
        return result;
    }

    public String getXmlInfoById(DynaBean dynaBean, String key) {
        if (dynaBean != null) {
            Object bytes = dynaBean.get(key);
            if (bytes == null) {
                return null;
            }
            if (bytes instanceof Blob) {
                try {
                    Blob blob = (Blob) bytes;
                    InputStream inputStream = blob.getBinaryStream();
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    return new String(outputStream.toByteArray(), Charsets.UTF_8);
                } catch (Exception e) {
                    // 处理异常
                    e.printStackTrace();
                    return null;
                }
            } else if (bytes instanceof byte[]) {
                // 如果已经是字节数组，直接转换
                return new String((byte[]) bytes, Charsets.UTF_8);
            }
        }
        return null;
    }


    protected Element getMxGeometryElementById(Element rootMxCellElement, String id) {
        for (Element cell : rootMxCellElement.elements()) {
            if (cell.attributeValue("id").equals(id)) {
                Element mxGeometryInfo = cell.element("mxCell").element("mxGeometry");
                return mxGeometryInfo;
            }
        }
        return null;
    }

    protected Element getElementById(Element rootMxCellElement, String id) {
        for (Element cell : rootMxCellElement.elements()) {
            if (cell.attributeValue("id").equals(id)) {
                return cell;
            }
        }
        return null;
    }

    protected double getElementX(Element element) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        return Double.valueOf(mxGeometryInfo.attributeValue("x", "0"));
    }


    protected double getElementY(Element element) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        return Double.valueOf(mxGeometryInfo.attributeValue("y", "0"));
    }


    protected void setElementX(Element element, double x) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        mxGeometryInfo.addAttribute("x", String.valueOf(x));
    }


    protected void setElementY(Element element, double y) {
        Element mxGeometryInfo = element.element("mxCell").element("mxGeometry");
        mxGeometryInfo.addAttribute("y", String.valueOf(y));
    }

    protected double getLineX(Element element) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        return Double.valueOf(mxGeometryInfo.attributeValue("x","0"));
    }


    protected double getLineY(Element element) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        return Double.valueOf(mxGeometryInfo.attributeValue("y","0"));
    }


    protected void setLineX(Element element, double x) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        mxGeometryInfo.addAttribute("x", String.valueOf(x));
    }


    protected void setLineY(Element element, double y) {
        Element mxGeometryInfo = element.element("mxGeometry").element("Array").element("mxPoint");
        mxGeometryInfo.addAttribute("y", String.valueOf(y));
    }

    protected Map<String, JSONObject> getChildShapesByJson(String json) {
        Map<String, JSONObject> result = new HashMap<>();
        JSONArray childShapes = JSONObject.parseObject(json).getJSONArray("childShapes");
        for (Object o : childShapes) {
            String resourceId = ((JSONObject) o).getString("resourceId");
            result.put(resourceId, (JSONObject) o);
        }
        return result;
    }


    public String getUuid() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 15);
        return uuid;
    }

    public Map<String, AbstractElementInfoVo> getAllNodeAndLine(String xml) {
        Map<String, AbstractElementInfoVo> result = new HashMap<>();

        Map<String, LineInfoVo> resultLine = new HashMap<>();
        Map<String, NodeInfoVo> resultNode = new HashMap<>();

        InputSource in = new InputSource(new StringReader(xml));
        // 创建 SAXReader 对象
        SAXReader reader = new SAXReader();
        // 读取 XML 文件
        Document document = null;
        try {
            document = reader.read(in);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        // 获取根元素
        Element root = document.getRootElement().element("root");
        for (Element cell : root.elements()) {
            if (cell.attributeValue("id").equals("0") || cell.attributeValue("id").equals("1")) {
                continue;
            }
            if (!Strings.isNullOrEmpty(cell.attributeValue("nodeType"))) {
                LineInfoVo lineInfo = buildElementToLine(cell);
                resultLine.put(lineInfo.getId(), lineInfo);
            } else {
                NodeInfoVo nodeInfo = buildElementToTask(cell);
                resultNode.put(nodeInfo.getId(), nodeInfo);
            }
        }

        for (String nodeKey : resultNode.keySet()) {
            NodeInfoVo nodeInfo = resultNode.get(nodeKey);
            for (String key : resultLine.keySet()) {
                LineInfoVo lineInfo = resultLine.get(key);
                if (lineInfo.getSource() != null && lineInfo.getSource().equals(nodeInfo.getId())) {
                    if (nodeInfo.getTargets() == null) {
                        nodeInfo.setTargets(new ArrayList<>());
                    }
                    nodeInfo.getTargets().add(lineInfo.getId());
                }
            }
        }

        result.putAll(resultNode);
        result.putAll(resultLine);
        return result;
    }

    private LineInfoVo buildElementToLine(Element element) {
        LineInfoVo lineInfo = new LineInfoVo();
        String id = element.attributeValue("id");
        String source = element.attributeValue("source");
        String target = element.attributeValue("target");
        lineInfo.setId(id);
        lineInfo.setSource(source);
        lineInfo.setTarget(target);
        return lineInfo;
    }

    private NodeInfoVo buildElementToTask(Element element) {
        NodeInfoVo nodeInfo = new NodeInfoVo();
        String id = element.attributeValue("id");
        String name = element.attributeValue("label");
        Element mxGeometry = element.element("mxCell").element("mxGeometry");
        String x = mxGeometry.attributeValue("x","0");
        String y = mxGeometry.attributeValue("y","0");
        String width = mxGeometry.attributeValue("width");
        String height = mxGeometry.attributeValue("height");
        nodeInfo.setId(id);
        nodeInfo.setName(name);
        nodeInfo.setX(x);
        nodeInfo.setY(y);
        nodeInfo.setWidth(width);
        nodeInfo.setHeight(height);
        return nodeInfo;
    }

}
