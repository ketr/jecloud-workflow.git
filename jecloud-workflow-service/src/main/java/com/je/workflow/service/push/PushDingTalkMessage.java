/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import com.google.common.base.Strings;
import com.je.bpm.core.model.config.ProcessRemindTemplateTypeEnum;
import com.je.bpm.core.model.config.process.ProcessRemindTemplate;
import com.je.workflow.service.push.pojo.CommonMessageVo;
import com.je.workflow.service.push.pojo.DingTalkMessageVo;
import com.je.workflow.service.push.pojo.PushMessageTypeEnum;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "dingtalkMessage")
public class PushDingTalkMessage extends AbstractPushMessageService {

    @Override
    public void execute(List<ProcessRemindTemplate> processRemindTemplates, String title,
                        String userName, String modelName, String submitType, String comment,
                        String userId, CommonMessageVo commonMessageVo, String thirdPartyContent, String customerContent) {
        String contentTemplate = "";
        for (ProcessRemindTemplate processRemindTemplate : processRemindTemplates) {
            if (!ProcessRemindTemplateTypeEnum.DINGTALK.getType().equals(processRemindTemplate.getType().getType())) {
                continue;
            }
            contentTemplate = processRemindTemplate.getTemplate();
        }

        String content = "";
        if (Strings.isNullOrEmpty(contentTemplate)) {
            content = "";
        } else {
            String urgency = commonMessageVo.getVariables().get("@URGENCY@");
            if (!Strings.isNullOrEmpty(urgency)) {
                if (urgency.equals("紧急")) {
                    commonMessageVo.getVariables().put("@URGENCY@", "<font color=\"red\">紧急</font>");
                } else if (urgency.equals("急")) {
                    commonMessageVo.getVariables().put("@URGENCY@", "<font color=\"orange\">急</font>");
                } else if (urgency.equals("一般")) {
                    commonMessageVo.getVariables().put("@URGENCY@", "<font color=\"green\">一般</font>");
                }
            }
            contentTemplate = WorkFlowVariable.formatVariable(contentTemplate, commonMessageVo.getVariables());
            content = CommonSystemVariable.formatVariable(contentTemplate);
        }
        if (Strings.isNullOrEmpty(title)) {
            title = String.format("流程[%s]提醒", modelName);
        } else {
            title = CommonSystemVariable.formatVariable(title);
        }
        if (!Strings.isNullOrEmpty(customerContent)) {
            content = customerContent;
        }

        DingTalkMessageVo dingTalkMessageVo = (DingTalkMessageVo) commonMessageVo;
        dingTalkMessageVo.setToUserId(userId);
        dingTalkMessageVo.setContent(content);
        dingTalkMessageVo.setTitle(title);
        RedisDelayQueue redisDelayQueue = new RedisDelayQueue(redisTemplate, delayQueueName);
        redisDelayQueue.setDelayQueue(dingTalkMessageVo,
                PushMessageTypeEnum.DING_TALK_PUSH_SEND.getName(), userId + "_" + title + "_" + content
                , System.currentTimeMillis());
    }
}
