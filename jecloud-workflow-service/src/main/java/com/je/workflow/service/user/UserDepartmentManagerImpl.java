/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.bpm.common.identity.ActivitiDepartment;
import com.je.bpm.runtime.shared.identity.BO.AssignmentPermissionBo;
import com.je.bpm.runtime.shared.identity.UserDepartmentManager;
import com.je.common.auth.AuthRealOrg;
import com.je.common.auth.impl.Department;
import com.je.common.auth.impl.RealOrganization;
import com.je.common.base.util.SecurityUserHolder;
import com.je.rbac.model.AssignmentPermission;
import com.je.rbac.rpc.TreatableUserRpcServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserDepartmentManagerImpl implements UserDepartmentManager {
    @Autowired
    private TreatableUserRpcServiceImpl treatableUserRpcService;

    @Override
    public List<ActivitiDepartment> findUserDepartments(String userId) {
        List<ActivitiDepartment> departments = new ArrayList<>();
        ActivitiDepartment department = new ActivitiDepartment();
        department.setId("1");
        department.setCode("dept1");
        department.setName("dept1");
        departments.add(department);
        return departments;
    }

    @Override
    public ActivitiDepartment findUserDepartments(String userId, String type) {
        return null;
    }

    @Override
    public ActivitiDepartment findLogUserDepartment() {
        AuthRealOrg authRealOrg = SecurityUserHolder.getCurrentAccount().getRealUser().getRealOrg();
        ActivitiDepartment activitiDepartment = new ActivitiDepartment();
        if (authRealOrg instanceof RealOrganization) {
            activitiDepartment.setCode(authRealOrg.getCode());
            activitiDepartment.setId(authRealOrg.getId());
            activitiDepartment.setName(authRealOrg.getName());
            activitiDepartment.setDeptId(authRealOrg.getId());
        }
        if (authRealOrg instanceof Department) {
            Department department = (Department) authRealOrg;
            activitiDepartment.setCode(department.getCode());
            activitiDepartment.setId(department.getId());
            activitiDepartment.setName(department.getName());
            activitiDepartment.setDeptId(department.getId());
        }
        return activitiDepartment;
    }

    @Override
    public ActivitiDepartment findDepartmentsById(String departmentId) {
        return null;
    }

    @Override
    public List<ActivitiDepartment> findDepartmentsByIds(Collection<String> departmentIds) {
        return null;
    }

    @Override
    public Object findUserDepts(String userId, String deptIds, AssignmentPermissionBo assignmentPermissionBo, Boolean orgFlag, Boolean multiple, Boolean addOwn) {
        AssignmentPermission assignmentPermission = new AssignmentPermission();
        if (assignmentPermissionBo != null) {
            BeanUtils.copyProperties(assignmentPermissionBo, assignmentPermission);
        }
        return treatableUserRpcService.findDeptUsersAndBuildTreeNode(userId, deptIds, assignmentPermission, orgFlag, multiple, addOwn);
    }

    @Override
    public Boolean checkContainsCurrentUser(String userId, String deptIds, AssignmentPermissionBo assignmentPermissionBo) {
        AssignmentPermission assignmentPermission = new AssignmentPermission();
        BeanUtils.copyProperties(assignmentPermissionBo, assignmentPermission);
        return treatableUserRpcService.checkDeptUsersContainsCurrentUser(userId, deptIds, assignmentPermission);
    }
}
