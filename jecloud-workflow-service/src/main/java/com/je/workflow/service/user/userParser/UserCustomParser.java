/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user.userParser;

import com.google.common.base.Strings;
import com.je.bpm.core.model.config.task.assignment.BasicAssignmentConfigImpl;
import com.je.bpm.core.model.config.task.assignment.CustomAssignmentConfigImpl;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.runtime.shared.identity.UserServeCustomizeManager;
import com.je.common.base.spring.SpringContextHolder;
import com.je.core.entity.extjs.JSONTreeNode;

import java.util.Map;

/**
 * 自定义
 */
public class UserCustomParser extends AbstractUserParser {

    @Override
    public JSONTreeNode parser(String userId, BasicAssignmentConfigImpl basicAssignmentConfig,
                               KaiteBaseUserTask kaiteBaseUserTask, Boolean showCompany, Map<String, Object> bean,
                               String prod, Boolean multiple, Boolean addOwn, Map<String, String> customerMap) {
        UserServeCustomizeManager userServeCustomizeManager = SpringContextHolder.getBean(UserServeCustomizeManager.class);
        CustomAssignmentConfigImpl customAssignmentConfig = (CustomAssignmentConfigImpl) basicAssignmentConfig;
        String operationId = customerMap != null ? customerMap.get("operationId") : "";
        if (Strings.isNullOrEmpty(operationId)) {
            return null;
        }
        String starterUser = customerMap != null ? customerMap.get("starterUser") : "";
        if (Strings.isNullOrEmpty(starterUser)) {
            return null;
        }
        Object child = userServeCustomizeManager.findUsers(customAssignmentConfig.getServiceName(),
                customAssignmentConfig.getMethodName(), kaiteBaseUserTask, prod, bean, multiple, addOwn, operationId, starterUser);
        if (child != null && child instanceof JSONTreeNode) {
            if (((JSONTreeNode) child).getCode().equals("9999")) {
                throw new ActivitiException("自定义获取人员信息异常！");
            }
            return (JSONTreeNode) child;
        }
        return null;
    }

    @Override
    public Boolean analysis(String userId, BasicAssignmentConfigImpl basicAssignmentConfig, KaiteBaseUserTask kaiteBaseUserTask,
                            Map<String, Object> bean, String prod, Map<String, String> customerMap, String operationId) {
        UserServeCustomizeManager userServeCustomizeManager = SpringContextHolder.getBean(UserServeCustomizeManager.class);
        CustomAssignmentConfigImpl customAssignmentConfig = (CustomAssignmentConfigImpl) basicAssignmentConfig;
        String starterUser = customerMap != null ? customerMap.get("starterUser") : "";
        if (Strings.isNullOrEmpty(starterUser)) {
            return null;
        }
        return userServeCustomizeManager.checkContainsCurrentUser(customAssignmentConfig.getServiceName(),
                customAssignmentConfig.getMethodName(), kaiteBaseUserTask, userId, prod, bean, operationId, starterUser);
    }

}
