/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.ibatis.extension.plugins.pagination.Page;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface MonitorService {

    Page load(BaseMethodArgument param, HttpServletRequest request);

    void clearDirtyData();

    Map<String, Object> getStatistics();

    void processAllHandover(String assignee, String toAssignee);

    void processHandoverByIds(String toAssignee, String ids);

    List<JSONObject> getRunNodes(String piid);

    List<JSONObject> getAllNode(String piid);

    String adjustRunningNode(String piid, String currentNodeId, String toNodeId, String prod, String tableCode,
                             String beanId, String funcCode, String funcId);

    String adjustNodeAssignee(String piid, String currentNodeId, String userIds, String userNames, String head, String headName, String prod, String tableCode, String beanId, String funcCode, String funcId);

    /**
     * 重启流程
     *
     * @param piid                  流程实例id
     * @param keepHistoricalRecords 是否保存历史审批数据
     */
    void restart(String piid, String beanId, boolean keepHistoricalRecords, String tableCode, String funcCode,String prod);

    JSONArray getHistoricalRecords(String beanId);

}
