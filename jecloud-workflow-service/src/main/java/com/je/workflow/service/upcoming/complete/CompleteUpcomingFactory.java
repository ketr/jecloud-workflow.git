/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete;

import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.common.base.spring.SpringContextHolder;
import com.je.workflow.service.upcoming.complete.impl.*;

import java.util.HashMap;
import java.util.Map;

public class CompleteUpcomingFactory {

    public static final Map<String, CompleteUpcomingService> map = new HashMap<>();

    static {
        //提交(完成)
        map.put(SubmitTypeEnum.SUBMIT.toString(), SpringContextHolder.getBean(SubmitUpcomingServiceImpl.class));
        //作废(完成)
        map.put(SubmitTypeEnum.INVALID.toString(), SpringContextHolder.getBean(InvalidUpcomingServiceImpl.class));
        //取回(完成)
        map.put(SubmitTypeEnum.RETRIEVE.toString(), SpringContextHolder.getBean(RetrieveUpcomingServiceImpl.class));
        //撤回(完成)
        map.put(SubmitTypeEnum.WITHDRAW.toString(), SpringContextHolder.getBean(WithdrawUpcomingServiceImpl.class));
        //退回(完成)
        map.put(SubmitTypeEnum.GOBACK.toString(), SpringContextHolder.getBean(GobackUpcomingServiceImpl.class));
        //直送(完成)
        map.put(SubmitTypeEnum.DIRECTSEND.toString(), SpringContextHolder.getBean(SubmitUpcomingServiceImpl.class));
        //驳回(完成)
        map.put(SubmitTypeEnum.DISMISS.toString(), SpringContextHolder.getBean(DismissUpcomingServiceImpl.class));
        //通过(完成)
        map.put(SubmitTypeEnum.PASS.toString(), SpringContextHolder.getBean(SubmitUpcomingServiceImpl.class));
        //改签(完成)
        map.put(SubmitTypeEnum.REBOOK.toString(), SpringContextHolder.getBean(SubmitUpcomingServiceImpl.class));
        //否决(完成)
        map.put(SubmitTypeEnum.VETO.toString(), SpringContextHolder.getBean(SubmitUpcomingServiceImpl.class));
        //弃权(完成)
        map.put(SubmitTypeEnum.ABSTAIN.toString(), SpringContextHolder.getBean(SubmitUpcomingServiceImpl.class));

        //加签减签
        map.put(SubmitTypeEnum.ADD.toString(), SpringContextHolder.getBean(AddUpcomingServiceImpl.class));
        map.put(SubmitTypeEnum.REDUCE.toString(), SpringContextHolder.getBean(ReduceUpcomingServiceImpl.class));
        //撤销(完成)
        map.put(SubmitTypeEnum.CANCEL.toString(), SpringContextHolder.getBean(CancelUpcomingServiceImpl.class));
        //结束
        map.put(SubmitTypeEnum.END.toString(), SpringContextHolder.getBean(EndUpcomingServiceImpl.class));
        //更换负责人(完成)
        map.put(SubmitTypeEnum.CHANGEASSIGNEE.toString(), SpringContextHolder.getBean(ChangeassigneeUpcomingServiceImpl.class));
        //领取(完成)
        map.put(SubmitTypeEnum.RECEIVE.toString(), SpringContextHolder.getBean(ReceiveUpcomingServiceImpl.class));
        //已阅(完成)
        map.put(SubmitTypeEnum.PASSROUNDREAD.toString(), SpringContextHolder.getBean(PassroundreadUpcomingServiceImpl.class));
        //发起(完成)
        map.put(SubmitTypeEnum.SPONSOR.toString(), SpringContextHolder.getBean(SponsorUpcomingServiceImpl.class));
        //转办(完成)
        map.put(SubmitTypeEnum.TRANSFER.toString(), SpringContextHolder.getBean(TransferUpcomingServiceImpl.class));
        //委托(完成)
        map.put(SubmitTypeEnum.DELEGATE.toString(), SpringContextHolder.getBean(DelegateUpcomingServiceImpl.class));
        //取消委托(完成)
        map.put(SubmitTypeEnum.CANCELDELEGATE.toString(), SpringContextHolder.getBean(CanceldelegateUpcomingServiceImpl.class));
        //传阅(完成)
        map.put(SubmitTypeEnum.PASSROUND.toString(), SpringContextHolder.getBean(PassroundUpcomingServiceImpl.class));
        //节点离开事件
        map.put(SubmitTypeEnum.OUTGOING.toString(), SpringContextHolder.getBean(OutgoingUpcomingServiceImpl.class));//节点离开事件
        //调整
        map.put(SubmitTypeEnum.ADJUST.toString(), SpringContextHolder.getBean(AdjustUpcomingServiceImpl.class));
    }

    public static CompleteUpcomingService getCompleteUpcomingServiceByType(String type) {
        return map.get(type);
    }

}
