/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import com.je.bpm.core.model.config.process.ProcessRemindTemplate;
import com.je.workflow.service.push.pojo.CommonMessageVo;
import org.springframework.stereotype.Service;
import java.util.List;

//@Service(value = "welinkMessage")
public class PushWeLinkMessage extends AbstractPushMessageService {

//    @Autowired
//    private WeLinkMsgRpcService weLinkMsgRpcService;

    @Override
    public void execute(List<ProcessRemindTemplate> processRemindTemplates, String title, String userName,
                        String modelName, String submitType, String comment, String userId,
                        CommonMessageVo commonMessageVo, String thirdPartyContent, String customerContent) {
        //TODO
//        MessageVo messageVo = new MessageVo();
//        messageVo.setUserIds(userId);
//        messageVo.setMsgType("WZ");
//        if (Strings.isNullOrEmpty(title)) {
//            messageVo.setMsgTitle("流程提醒");
//        } else {
//            String overTitle = CommonSystemVariable.formatVariable(title);
//            messageVo.setMsgTitle(overTitle);
//        }
//        messageVo.setUrlPath("");
//        if (Strings.isNullOrEmpty(thirdPartyContent)) {
//            thirdPartyContent = String.format("由【%s】在【%s】给您提交了【%s】任务, 执行操作：【%s】,执行意见：【%s】",
//                    userName, DateUtil.now(), modelName, submitType, comment);
//            if (Strings.isNullOrEmpty(comment)) {
//                thirdPartyContent = String.format("由【%s】在【%s】给您提交了【%s】任务, 执行操作：【%s】",
//                        userName, DateUtil.now(), modelName, submitType);
//            }
//            messageVo.setMsgContent(thirdPartyContent);
//        } else {
//            messageVo.setMsgContent(thirdPartyContent);
//        }
//        if (!Strings.isNullOrEmpty(customerContent)) {
//            messageVo.setContent(customerContent);
//        }
//        try {
//            weLinkMsgRpcService.send(messageVo);
//        } catch (MessageSendException e) {
//            e.printStackTrace();
//        }
    }
}
