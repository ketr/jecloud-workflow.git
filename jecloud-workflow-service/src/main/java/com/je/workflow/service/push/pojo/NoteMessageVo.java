/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push.pojo;

public class NoteMessageVo extends CommonMessageVo {

    private String userPhone;
    private String toUserName;
    private String sourceUserName;
    /**
     * 服务商
     */
    protected String serviceType;
    /**
     * 签名
     */
    protected String signName;
    /**
     * 模版编码
     */
    protected String templateCode;

    public NoteMessageVo(String toUserId, String userPhone, String toUserName, String sourceUserName,
                         String sourceUserId, String content, String serviceType, String signName, String templateCode) {
        super.setToUserId(toUserId);
        super.setSourceUserId(sourceUserId);
        super.setContent(content);
        this.userPhone = userPhone;
        this.toUserName = toUserName;
        this.sourceUserName = sourceUserName;
        this.signName = signName;
        this.templateCode = templateCode;
        this.serviceType = serviceType;
    }


    public NoteMessageVo(String toUserId, String userPhone, String toUserName, String sourceUserName, String sourceUserId,
                         String serviceType, String signName, String templateCode) {
        this(toUserId, userPhone, toUserName, sourceUserName, sourceUserId, "", serviceType, signName, templateCode);
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getSourceUserName() {
        return sourceUserName;
    }

    public void setSourceUserName(String sourceUserName) {
        this.sourceUserName = sourceUserName;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public String toString() {
        return "NoteMessageVo{" +
                "userPhone='" + userPhone + '\'' +
                ", toUserName='" + toUserName + '\'' +
                ", sourceUserName='" + sourceUserName + '\'' +
                '}';
    }
}
