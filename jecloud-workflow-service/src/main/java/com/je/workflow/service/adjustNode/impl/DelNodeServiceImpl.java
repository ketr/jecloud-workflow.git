package com.je.workflow.service.adjustNode.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.workflow.service.adjustNode.DelNodeService;
import com.je.workflow.service.adjustNode.vo.AbstractElementInfoVo;
import com.je.workflow.service.adjustNode.vo.DelNodeParamsVo;
import com.je.workflow.service.adjustNode.vo.LineInfoVo;
import com.je.workflow.service.adjustNode.vo.NodeInfoVo;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DelNodeServiceImpl extends AbstractAdjustNodeService implements DelNodeService {

    @Override
    public File delNode(DelNodeParamsVo vo) {
        Map<String, String> xmlAndJson = getXmlAndJson(vo.getBeanId(), vo.getPdid());
        String xml = xmlAndJson.get("metaInfoXml");

        String[] delNodeArray = vo.getTaskNodeIds().split(",");

        Document document = xmlToDocument(xml);

        for (String nodeId : delNodeArray) {
            delByNodeId(document, nodeId);
        }
        xml = document.asXML();

        //修改json
        String json = xmlAndJson.get("metaInfo");
        Map<String, AbstractElementInfoVo> map = getAllNodeAndLine(xml);
//        return modelService.getModelSvgByXml(xml);
        String updateJson = updateJsonByXml(json, map, delNodeArray);
        return updateCsInfo(updateJson, xml, vo.getPdid(), vo.getBeanId(), vo.getPiid());
    }


    public String updateJsonByXml(String json, Map<String, AbstractElementInfoVo> allElement, String[] delNodeArray) {
        //获取json中所有的node和line，对比map中的node和line
        Map<String, JSONObject> childShapes = getChildShapesByJson(json);
        for (String delNode : delNodeArray) {
            childShapes.remove(delNode);
        }

        for (String key : allElement.keySet()) {
            //找不到就增加，找到就修改
            JSONObject jsonElement = childShapes.get(key);
            AbstractElementInfoVo elementInfo = allElement.get(key);
            if (jsonElement != null) {//修改
                //如果是节点,目前可以修改的只有位置
                if (elementInfo.getType().equals(NodeInfoVo.TYPE)) {
                    NodeInfoVo nodeInfo = (NodeInfoVo) elementInfo;
                    jsonElement.put("x", nodeInfo.getX());
                    jsonElement.put("y", nodeInfo.getY());
                    JSONArray target = jsonElement.getJSONArray("target");
                    target.clear();
                    if (nodeInfo.getTargets() != null && nodeInfo.getTargets().size() > 0) {
                        for (String tar : nodeInfo.getTargets()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("resourceId", tar);
                            target.add(jsonObject);
                        }
                    }
                }
                //如果是线
                if (elementInfo.getType().equals(LineInfoVo.TYPE)) {
                    LineInfoVo lineInfo = (LineInfoVo) elementInfo;
                    JSONArray target = jsonElement.getJSONArray("target");
                    if (target != null) {
                        target.getJSONObject(0).put("resourceId", lineInfo.getTarget());
                    }

                    JSONArray source = jsonElement.getJSONArray("source");
                    if (source != null) {
                        source.getJSONObject(0).put("resourceId", lineInfo.getSource());
                    }
                }
            }
        }
        return setChildShapes(json, childShapes);
    }

    protected String setChildShapes(String json, Map<String, JSONObject> childShapes) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONArray jsonArray = new JSONArray();
        for (String key : childShapes.keySet()) {
            jsonArray.add(childShapes.get(key));
        }
        jsonObject.put("childShapes", jsonArray);
        return jsonObject.toString();
    }

    private Document xmlToDocument(String xml) {
        InputSource in = new InputSource(new StringReader(xml));
        // 创建 SAXReader 对象
        SAXReader reader = new SAXReader();
        // 读取 XML 文件
        Document document = null;
        try {
            document = reader.read(in);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return document;
    }

    private void delByNodeId(Document document, String nodeId) {
        // 获取根元素
        Element rootMxCellElement = document.getRootElement().element("root");
        //改之前线的source ID
        //要删除的节点
        Element element = getElementById(rootMxCellElement, nodeId);
        String delLineId = "";
        String updateLineId = "";
        for (Element cell : rootMxCellElement.elements()) {
            //更改的线 删除节点的出线
            if (!Strings.isNullOrEmpty(cell.attributeValue("source")) && cell.attributeValue("source").equals(nodeId)) {
                updateLineId = cell.attributeValue("id");
            }
            //删除的线  删除节点的入线
            if (!Strings.isNullOrEmpty(cell.attributeValue("target")) && cell.attributeValue("target").equals(nodeId)) {
                delLineId = cell.attributeValue("id");
            }
        }

        Element delLin = getElementById(rootMxCellElement, delLineId);
        //把节点的出线，改成删除节点的出线
        String updateNodeId = delLin.attributeValue("source");
        Element updateLine = getElementById(rootMxCellElement, updateLineId);
        updateLine.addAttribute("source", updateNodeId);

        //所有节点往前移
        coordinateMovement(rootMxCellElement, nodeId);
        rootMxCellElement.remove(delLin);
        rootMxCellElement.remove(element);
    }


    private void coordinateMovement(Element rootMxCellElement, String delNodeId) {
        Element toNode = null;
        List<Element> tasks = new ArrayList<>();
        List<Element> lines = new ArrayList<>();
        for (Element cell : rootMxCellElement.elements()) {
            if (!Strings.isNullOrEmpty(cell.attributeValue("type"))) {
                tasks.add(cell);
                if (cell.attributeValue("id").equals(delNodeId)) {
                    toNode = cell;
                }
            }
            if (!Strings.isNullOrEmpty(cell.attributeValue("nodeType")) && cell.attributeValue("nodeType").equals("line")) {
                Element element = cell.element("mxGeometry").element("Array");
                if (element == null) {
                    continue;
                }
                lines.add(cell);
                lines.add(cell);
            }
        }

        double toNodeX = getElementX(toNode);
        double toNodeY = getElementY(toNode);
        // 设定统一移动的距离
        int moveDistance = 140;  // 例如：统一向右移动 100 个单位
        // 遍历所有节点，修改x轴坐标
        List<String> moveTasks = new ArrayList<>();
        for (Element task : tasks) {
            if (task.element("mxCell").element("mxGeometry") == null) {
                continue;
            }
            double taskNodeX = getElementX(task);
            double taskNodeY = getElementY(task);
            double difference = toNodeY - taskNodeY;
            // 如果节点在新节点的右边（x 大于新节点的 x）且 y 坐标一致
            if (taskNodeX - toNodeX > 20) {
                // 统一向右移动
                moveTasks.add(task.attributeValue("id"));
//                System.out.println(String.format("当前节点%s,当前位置%s,移动位置%s", task.attributeValue("name"), taskNodeX, taskNodeX + moveDistance));
                setElementX(task, taskNodeX - moveDistance);
            }
        }

        //遍历所有线，修改x轴坐标
        for (Element line : lines) {
            if (moveTasks.contains(line.attributeValue("source"))) {
                setLineX(line, getLineX(line) - 80);
            }
        }
    }

    private void updateLineSource(Element rootMxCellElement, String nodeId) {

    }

}
