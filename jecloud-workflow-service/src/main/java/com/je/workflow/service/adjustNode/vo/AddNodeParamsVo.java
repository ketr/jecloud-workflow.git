package com.je.workflow.service.adjustNode.vo;

import java.util.List;

/**
 * 添加节点参数 vo
 */
public class AddNodeParamsVo {

    public static AddNodeParamsVo build() {
        return new AddNodeParamsVo();
    }

    /**
     * 流程部署id
     */
    private String pdid;
    /**
     * 流程实例id
     */
    private String piid;
    /**
     * 业务主键
     */
    private String beanId;
    /**
     * 加签的人员信息
     */
    private List<AdjustSerialUserInfoVo> users;
    /**
     * 加签开始节点
     */
    private String toSourceId;
    /**
     * 加签结束节点，有多个必须传，有一个可以不传
     */
    private String toTargetId;


    public AddNodeParamsVo pdid(String pdid) {
        this.pdid = pdid;
        return this;
    }

    public AddNodeParamsVo beanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public AddNodeParamsVo users(List<AdjustSerialUserInfoVo> users) {
        this.users = users;
        return this;
    }

    public AddNodeParamsVo toSourceId(String toSourceId) {
        this.toSourceId = toSourceId;
        return this;
    }

    public AddNodeParamsVo toTargetId(String toTargetId) {
        this.toTargetId = toTargetId;
        return this;
    }

    public String getPiid() {
        return piid;
    }

    public void setPiid(String piid) {
        this.piid = piid;
    }

    public String getPdid() {
        return pdid;
    }

    public void setPdid(String pdid) {
        this.pdid = pdid;
    }

    public String getBeanId() {
        return beanId;
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    public List<AdjustSerialUserInfoVo> getUsers() {
        return users;
    }

    public void setUsers(List<AdjustSerialUserInfoVo> users) {
        this.users = users;
    }

    public String getToSourceId() {
        return toSourceId;
    }

    public void setToSourceId(String toSourceId) {
        this.toSourceId = toSourceId;
    }

    public String getToTargetId() {
        return toTargetId;
    }

    public void setToTargetId(String toTargetId) {
        this.toTargetId = toTargetId;
    }
}
