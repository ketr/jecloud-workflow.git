package com.je.workflow.service.adjustNode.vo;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 调整节点人员信息
 */
public class AdjustSerialUserInfoVo {
    /**
     * 用户id
     */
    private String id;
    /**
     * 用户唯一id
     */
    private String resourceId;
    /**
     * 用户编码
     */
    private String resourceCode;
    /**
     * 用户名称
     */
    private String resourceName;
    /**
     * 节点id
     */
    private String nodeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public static List<AdjustSerialUserInfoVo> buildStringToList(String json) {
        List<AdjustSerialUserInfoVo> list = new ArrayList<>();
        JSONArray jsonArray = JSONArray.parseArray(json);
        for (Object o : jsonArray) {
            AdjustSerialUserInfoVo addSerialUserInfo = new AdjustSerialUserInfoVo();
            JSONObject jsonObject = (JSONObject) o;
            addSerialUserInfo.setId(jsonObject.getString("id"));
            addSerialUserInfo.setResourceCode(jsonObject.getString("resourceCode"));
            addSerialUserInfo.setResourceId(jsonObject.getString("resourceId"));
            addSerialUserInfo.setResourceName(jsonObject.getString("resourceName"));
            list.add(addSerialUserInfo);
        }
        return list;
    }

}
