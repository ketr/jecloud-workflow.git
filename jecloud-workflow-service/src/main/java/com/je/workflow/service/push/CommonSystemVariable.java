/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import com.je.common.base.project.Project;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.ProjectContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class CommonSystemVariable {

    public static Map<String, Object> systemVariable() {
        Map<String, Object> variables = new HashMap<>(64);

        //------- 用户基础信息
        variables.put("@USER_ID@", SecurityUserHolder.getCurrentAccountRealUser() == null ? "" : SecurityUserHolder.getCurrentAccountRealUserId());
        variables.put("@USER_CODE@", SecurityUserHolder.getCurrentAccountRealUser() == null ? "" : SecurityUserHolder.getCurrentAccountRealUser().getCode());
        variables.put("@USER_NAME@", SecurityUserHolder.getCurrentAccountRealUser() == null ? "" : SecurityUserHolder.getCurrentAccountRealUserName());

        variables.put("@USER_PHONE@", SecurityUserHolder.getCurrentAccountRealUser() == null ? "" : SecurityUserHolder.getCurrentAccountRealUser().getPhone());
        variables.put("@USER_EMAIL@", SecurityUserHolder.getCurrentAccountRealUser() == null ? "" : SecurityUserHolder.getCurrentAccountRealUser().getEmail());

        variables.put("@DEPT_ID@", SecurityUserHolder.getCurrentAccountDepartment() == null ? "" : SecurityUserHolder.getCurrentAccountDepartment().getId());
        variables.put("@DEPT_CODE@", SecurityUserHolder.getCurrentAccountDepartment() == null ? "" : SecurityUserHolder.getCurrentAccountDepartment().getCode());
        variables.put("@DEPT_NAME@", SecurityUserHolder.getCurrentAccountDepartment() == null ? "" : SecurityUserHolder.getCurrentAccountDepartment().getName());

        //------- 时间变量
        variables.put("@NOW_DATE@", DateUtils.formatDate(new Date()));
        variables.put("@NOW_MONTH@", DateUtils.formatDate(new Date(), "yyyy-MM"));
        variables.put("@NOW_TIME@", DateUtils.formatDateTime(new Date()));
        variables.put("@NOW_YEAR@", DateUtils.formatDate(new Date(), "yyyy"));
        variables.put("@NOW_ONLYMONTH@", DateUtils.formatDate(new Date(), "MM"));
        Project project = ProjectContextHolder.getProject();
        if (project != null) {
            variables.put("@PROJECT_CODE@", project.getCode());
            variables.put("@PROJECT_ID@", project.getId());
            variables.put("@PROJECT_NAME@", project.getName());
            variables.put("@PROJECT_ORG_CODE@", project.getOrgName());
            variables.put("@PROJECT_ORG_ID@", project.getOrgId());
            variables.put("@PROJECT_ORG_NAME@", project.getOrgName());
        }
        return variables;
    }

    public static String formatVariable(String str) {
        Map<String, Object> variables = new HashMap<>(10);

        //添加默认变量
        Map<String, Object> systemVariable = systemVariable();
        Optional.ofNullable(systemVariable).ifPresent(variables::putAll);
        for (Map.Entry<String, Object> entry : systemVariable.entrySet()) {
            String key = entry.getKey();
            if (str.indexOf(key) != -1) {
                str = str.replaceAll("\\{" + key + "\\}", entry.getValue().toString());
            }
        }
        return str;
    }
}
