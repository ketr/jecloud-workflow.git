/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class WorkFlowVariable {

    public static String formatVariable(String str, Map<String, String> systemVariable) {
        if (systemVariable == null || systemVariable.size() == 0) {
            return str;
        }
        Map<String, Object> variables = new HashMap<>();
        //添加默认变量
//        variables = CommonSystemVariable.systemVariable();
        Optional.ofNullable(systemVariable).ifPresent(variables::putAll);
        //值集合
        for (Map.Entry<String, String> entry : systemVariable.entrySet()) {
            String key = entry.getKey();
            if (str.indexOf(key) != -1) {
                if (entry.getValue() != null) {
                    str = str.replaceAll("\\{" + key + "\\}", entry.getValue());
                } else {
                    str = str.replaceAll("\\{" + key + "\\}", "");
                }
            }
        }
        return str;
    }
}
