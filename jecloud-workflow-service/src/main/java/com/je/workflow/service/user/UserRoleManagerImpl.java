/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.bpm.common.identity.ActivitiRole;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.runtime.shared.identity.BO.AssignmentPermissionBo;
import com.je.bpm.runtime.shared.identity.UserRoleManager;
import com.je.common.auth.impl.account.Account;
import com.je.rbac.model.AssignmentPermission;
import com.je.rbac.rpc.TreatableUserRpcServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserRoleManagerImpl implements UserRoleManager {
    @Autowired
    private TreatableUserRpcServiceImpl treatableUserRpcService;

    @Override
    public List<ActivitiRole> findUserRoles(String userId) {
        List<ActivitiRole> roles = new ArrayList<>();
        ActivitiRole role = new ActivitiRole();
        role.setId("1");
        role.setCode("role1");
        role.setName("role1");
        roles.add(role);
        return roles;
    }

    @Override
    public Boolean logUserRoleIsInRoleIds(String userId, String roleIds) {
        Account account = (Account) Authentication.getAuthenticatedUser();
        List<String> list = account.getRoleIds();
        for (String str : list) {
            if (roleIds.contains(str)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object findUserRoles(String userId, String roleIds, AssignmentPermissionBo assignmentPermissionBo, Boolean aBoolean, Boolean multiple, Boolean addOwn) {
        AssignmentPermission assignmentPermission = new AssignmentPermission();
        if (assignmentPermissionBo != null) {
            BeanUtils.copyProperties(assignmentPermissionBo, assignmentPermission);
        }
        return treatableUserRpcService.findRoleUsersAndBuildTreeNode(userId, roleIds, assignmentPermission, aBoolean, multiple, addOwn);
    }

    @Override
    public ActivitiRole findRole(String roleId) {
        return null;
    }

    @Override
    public List<ActivitiRole> findRoles(Collection<String> roleIds) {
        return null;
    }

    @Override
    public Boolean checkContainsCurrentUser(String userId, String roleIds, AssignmentPermissionBo assignmentPermissionBo) {
        AssignmentPermission assignmentPermission = new AssignmentPermission();
        if (assignmentPermissionBo != null) {
            BeanUtils.copyProperties(assignmentPermissionBo, assignmentPermission);
        }
        return treatableUserRpcService.checkRoleContainsCurrentUser(userId, roleIds, assignmentPermission);
    }
}
