/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.controller.procerss;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.rbac.annotation.ControllerAuditLog;
import com.je.workflow.service.usertask.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 流程监控
 */
@RestController
@RequestMapping(value = "/je/workflow/monitor")
public class MonitorController extends AbstractPlatformController {

    @Autowired
    private MonitorService monitorService;

    @Override
    @RequestMapping(
            value = {"/load"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "重写流程监控load方法"
    )
    @ControllerAuditLog(moduleName = "流程监控模块", operateTypeCode = "loadWorkflow", operateTypeName = "查看流程监控", logTypeCode = "systemManage", logTypeName = "系统管理")
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        Page page = monitorService.load(param, request);
        return page == null ? BaseRespResult.successResultPage(Lists.newArrayList(), 0L)
                : BaseRespResult.successResultPage(page.getRecords(), (long) page.getTotal());
    }

    @RequestMapping(
            value = {"/clearDirtyData"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "清理流程待办脏数据"
    )
    @ControllerAuditLog(moduleName = "流程监控模块", operateTypeCode = "clearWorkflowDirty", operateTypeName = "清理流程待办脏数据", logTypeCode = "systemManage", logTypeName = "系统管理")
    public void clearDirtyData() {
        monitorService.clearDirtyData();
    }

    @RequestMapping(
            value = {"/getStatistics"},
            method = {RequestMethod.GET},
            produces = {"application/json; charset=utf-8"},
            name = "流程监控统计"
    )
    public BaseRespResult getStatistics() {
        Map<String, Object> result = monitorService.getStatistics();
        return BaseRespResult.successResult(result);
    }

    @RequestMapping(
            value = {"/processHandover"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "流程交接"
    )
    public BaseRespResult processHandover(BaseMethodArgument param, HttpServletRequest request) {
        String assignee = getStringParameter(request, "assignee");
        String toAssignee = getStringParameter(request, "toAssignee");
        String type = getStringParameter(request, "type");
        String ids = getStringParameter(request, "ids");
        if (type.equals("all")) {
            if (Strings.isNullOrEmpty(assignee)) {
                return BaseRespResult.errorResult("请指定待办人员。");
            }
            monitorService.processAllHandover(assignee, toAssignee);
        } else {
            if (Strings.isNullOrEmpty(ids)) {
                return BaseRespResult.errorResult("请指定交接数据。");
            }
            if (Strings.isNullOrEmpty(ids) || Strings.isNullOrEmpty(toAssignee)) {
                return BaseRespResult.errorResult("请指定交接人员。");
            }
            monitorService.processHandoverByIds(toAssignee, ids);
        }
        return BaseRespResult.successResult("交接成功！");
    }

    @RequestMapping(
            value = {"/getRunNodes"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "获取正在运转的节点"
    )
    public BaseRespResult getRunNodes(BaseMethodArgument param, HttpServletRequest request) {
        String piid = getStringParameter(request, "piid");
        return BaseRespResult.successResult(monitorService.getRunNodes(piid));
    }

    @RequestMapping(
            value = {"/getAllNode"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "获取所有节点"
    )
    public BaseRespResult getAllNode(BaseMethodArgument param, HttpServletRequest request) {
        String piid = getStringParameter(request, "piid");
        return BaseRespResult.successResult(monitorService.getAllNode(piid));
    }

    @RequestMapping(
            value = {"/adjustRunningNode"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "调整运行节点"
    )
    public BaseRespResult adjustRunningNode(BaseMethodArgument param, HttpServletRequest request) {
        String piid = getStringParameter(request, "piid");
        String currentNodeId = getStringParameter(request, "currentNodeId");
        String toNodeId = getStringParameter(request, "toNodeId");
        String prod = getStringParameter(request, "prod");
        String tableCode = getStringParameter(request, "tableCode");
        String beanId = getStringParameter(request, "beanId");
        String funcCode = getStringParameter(request, "funcCode");
        String funcId = getStringParameter(request, "funcId");
        return BaseRespResult.successResult(monitorService.adjustRunningNode(piid, currentNodeId, toNodeId, prod,
                tableCode, beanId, funcCode, funcId));
    }

    @RequestMapping(
            value = {"/adjustRunningNodeAssignee"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "调整节点处理人"
    )
    public BaseRespResult adjustNodeAssignee(BaseMethodArgument param, HttpServletRequest request) {
        String piid = getStringParameter(request, "piid");
        //当前节点id
        String currentNodeId = getStringParameter(request, "currentNodeId");
        //调整人员id
        String userIds = getStringParameter(request, "userIds");
        //调正人员name
        String userNames = getStringParameter(request, "userNames");
        //负责人ID
        String head = getStringParameter(request, "head");
        //负责人Name
        String headName = getStringParameter(request, "headName");
        String prod = getStringParameter(request, "prod");
        String tableCode = getStringParameter(request, "tableCode");
        String beanId = getStringParameter(request, "beanId");
        String funcCode = getStringParameter(request, "funcCode");
        String funcId = getStringParameter(request, "funcId");
        return BaseRespResult.successResult(monitorService.adjustNodeAssignee(piid, currentNodeId, userIds, userNames, head, headName, prod,
                tableCode, beanId, funcCode, funcId));
    }

    @RequestMapping(value = {"/restart"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"}, name = "流程重新启动")
    public BaseRespResult restart(BaseMethodArgument param, HttpServletRequest request) {
        String piid = getStringParameter(request, "piid");
        String beanId = getStringParameter(request, "beanId");
        String keepHistoricalRecordsVar = getStringParameter(request, "keepHistoricalRecords");
        String funcCode = getStringParameter(request, "funcCode");
        String tableCode = getStringParameter(request, "tableCode");
        String prod = getStringParameter(request, "prod");

        boolean keepHistoricalRecords = true;
        if (!Strings.isNullOrEmpty(keepHistoricalRecordsVar) && keepHistoricalRecordsVar.equals("0")) {
            keepHistoricalRecords = false;
        }
        monitorService.restart(piid, beanId, keepHistoricalRecords, tableCode, funcCode, prod);
        return BaseRespResult.successResult("重启成功！");
    }


}

