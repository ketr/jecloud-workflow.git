package com.je.workflow.controller.procerss;

import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.workflow.service.adjustNode.AddNodeService;
import com.je.workflow.service.adjustNode.DelNodeService;
import com.je.workflow.service.adjustNode.vo.AddNodeParamsVo;
import com.je.workflow.service.adjustNode.vo.AdjustSerialUserInfoVo;
import com.je.workflow.service.adjustNode.vo.DelNodeParamsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 加签（业务人员使用）
 */

@RestController
@RequestMapping(value = "/je/workflow/countersign")
public class AddNodeController {

    @Autowired
    private AddNodeService addNodeService;
    @Autowired
    private DelNodeService delNodeService;

    /**
     * 获取流程按钮操作参数详情
     */
    @RequestMapping(value = "/addNode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getParams(BaseMethodArgument param, HttpServletRequest request) {
        String beanId = request.getParameter("beanId");
        String pdid = request.getParameter("pdid");
        String sourceId = request.getParameter("sourceId");
        String targetId = request.getParameter("targetId");
        List<AdjustSerialUserInfoVo> userInfoVoList = AdjustSerialUserInfoVo.buildStringToList(request.getParameter("users"));
        AddNodeParamsVo vo = AddNodeParamsVo.build()
                .pdid(pdid).beanId(beanId).toSourceId(sourceId)
                .toTargetId(targetId).users(userInfoVoList);
        File file = addNodeService.addSerialNode(vo);
        return BaseRespResult.successResult("加签成功！");
    }

    @RequestMapping(value = "/delNode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult delNode(BaseMethodArgument param, HttpServletRequest request) {
        String beanId = request.getParameter("beanId");
        String pdid = request.getParameter("pdid");
        String taskNodeIds = request.getParameter("taskNodeIds");
        DelNodeParamsVo vo = DelNodeParamsVo.build()
                .pdid(pdid).beanId(beanId).taskNodeIds(taskNodeIds);
        File file = delNodeService.delNode(vo);
        return BaseRespResult.successResult("加签成功！");
    }


    /**
     * 获取加签节点
     */
    @RequestMapping(value = "/getAddNodes", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getAddNodes(BaseMethodArgument param, HttpServletRequest request) {
        String beanId = request.getParameter("beanId");
        String taskId = request.getParameter("taskId");
        String piid = request.getParameter("piid");
        List<Map<String, String>> list = addNodeService.getAddNodes(beanId, taskId, piid);
        return BaseRespResult.successResult(list);
    }

    /**
     * 获取加签节点
     */
    @RequestMapping(value = "/getCsInfo", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getCsInfo(BaseMethodArgument param, HttpServletRequest request) {
        String beanId = request.getParameter("beanId");
        String pdid = request.getParameter("pdid");
        HashMap<String, String> list = delNodeService.getXmlAndJson(beanId, pdid);
        return BaseRespResult.successResult(list);
    }

}
