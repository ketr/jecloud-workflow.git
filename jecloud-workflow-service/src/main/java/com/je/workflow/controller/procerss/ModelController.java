/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.controller.procerss;

import com.google.common.base.Strings;
import com.je.bpm.engine.ActivitiException;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.workflow.service.model.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 流程模型控制层
 */

@RestController
@RequestMapping(value = "/je/workflow/model")
public class ModelController extends AbstractPlatformController {

    @Autowired
    private ModelService modelService;

    /**
     * 保存模型
     */
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String id = dynaBean.getPkValue();
        if (Strings.isNullOrEmpty(id)) {
            String metaInfoJson = request.getParameter("metaInfo");
            if (Strings.isNullOrEmpty(metaInfoJson)) {
                return super.doSave(param, request);
            }
            String metaInfoXml = getStringParameter(request, "metaInfoXml");
            modelService.doSave(dynaBean, metaInfoJson, metaInfoXml);
            request.setAttribute("dynaBean", dynaBean);
            HashMap<String, Object> meta = (HashMap<String, Object>) super.doSave(param, request).getData();
            meta.put("metaInfo", modelService.getJsonInfoById((String) meta.get("PROCESSINFO_MODEL_ID")));
            meta.put("metaInfoXml", modelService.getXmlInfoById((String) meta.get("PROCESSINFO_RESOURCE_XML_ID")));
            return BaseRespResult.successResult(meta);
        } else {
            return doUpdate(param, request);
        }
    }

    /**
     * 修改模型
     */
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        HashMap<String, Object> meta = (HashMap<String, Object>) super.getInfoById(param, request).getData();
        String modelId = (String) meta.get("PROCESSINFO_MODEL_ID");
        String xmlId = (String) meta.get("PROCESSINFO_RESOURCE_XML_ID");
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String metaInfoJson = request.getParameter("metaInfo");
        String metaInfoXml = getStringParameter(request, "metaInfoXml");
        modelService.doUpdate(modelId, xmlId, dynaBean, metaInfoJson, metaInfoXml);
        request.setAttribute("dynaBean", dynaBean);
        meta = (HashMap<String, Object>) super.doUpdate(param, request).getData();
        meta.put("metaInfo", modelService.getJsonInfoById((String) meta.get("PROCESSINFO_MODEL_ID")));
        meta.put("metaInfoXml", modelService.getXmlInfoById((String) meta.get("PROCESSINFO_RESOURCE_XML_ID")));
        return BaseRespResult.successResult(meta);
    }

    /**
     * 根据modelId，获取流程模型详细信息
     */
    @RequestMapping(value = "/getInfoById", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult getInfoById(BaseMethodArgument param, HttpServletRequest request) {
        BaseRespResult baseRespResult = super.getInfoById(param, request);
        HashMap<String, Object> meta = (HashMap<String, Object>) baseRespResult.getData();
        meta.put("metaInfo", modelService.getJsonInfoById((String) meta.get("PROCESSINFO_MODEL_ID")));
        meta.put("metaInfoXml", modelService.getXmlInfoById((String) meta.get("PROCESSINFO_RESOURCE_XML_ID")));
        return BaseRespResult.successResult(meta);
    }

    @RequestMapping(value = "/getInfoByPiid", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getInfoByPiid(BaseMethodArgument param, HttpServletRequest request) {
        String piid = request.getParameter("SY_PIID");
        String pdid = request.getParameter("SY_PDID");
        HashMap<String, Object> values = modelService.getInfoByPiid(piid, pdid);
        return BaseRespResult.successResult(values);
    }

    @RequestMapping(value = "/getHistoryInfoByPlanningLogId", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getHistoryInfoByPlanningLogId(BaseMethodArgument param, HttpServletRequest request) {
        String id = request.getParameter("JE_WORKFLOW_RN_PLANNING_LOG_ID");
        String type = request.getParameter("type");
        HashMap<String, Object> values = modelService.getHistoryInfoByPlanningLogId(id,type);
        return BaseRespResult.successResult(values);
    }


    /**
     * 保存并发布模型-运行的
     */
    @RequestMapping(value = "/updateAndDeployByPiid", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult updateAndDeployByPiid(BaseMethodArgument param, HttpServletRequest request) {
        String piid = request.getParameter("SY_PIID");
        String pdid = request.getParameter("SY_PDID");
        String LOG_ADDINFO = request.getParameter("LOG_ADDINFO");
        String LOG_UPDATEINFO = request.getParameter("LOG_UPDATEINFO");
        String LOG_DELETEINFO = request.getParameter("LOG_DELETEINFO");
        String LOG_XGJCXX = request.getParameter("LOG_XGJCXX");
        String metaInfoJson = request.getParameter("metaInfo");
        String metaInfoXml = getStringParameter(request, "metaInfoXml");
        HashMap<String, Object> result = modelService.doUpdateRunProcessInfo(piid, pdid, LOG_ADDINFO, LOG_UPDATEINFO, LOG_DELETEINFO, LOG_XGJCXX, metaInfoJson, metaInfoXml);
        return BaseRespResult.successResult(result, "保存并发布成功！");
    }


    /**
     * 部署模型
     */
    @RequestMapping(value = "/deploy", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult deploy(BaseMethodArgument param, HttpServletRequest request) {
        HashMap<String, Object> meta = (HashMap<String, Object>) super.getInfoById(param, request).getData();
        String id = param.getPkValue();
        if (Strings.isNullOrEmpty(id)) {
            throw new ActivitiException("processInfoId信息为空！");
        }
        meta.put("metaInfo", modelService.getJsonInfoById((String) meta.get("PROCESSINFO_MODEL_ID")));
        meta.put("metaInfoXml", modelService.getXmlInfoById((String) meta.get("PROCESSINFO_RESOURCE_XML_ID")));
        modelService.deploy(id);
        return BaseRespResult.successResult(meta, "部署成功！");
    }


    /**
     * 保存并发布模型
     */
    @RequestMapping(value = "/updateAndDeploy", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult updateAndDeploy(BaseMethodArgument param, HttpServletRequest request) {
        BaseRespResult baseRespResult = new BaseRespResult();
        String id = param.getPkValue();
        if (Strings.isNullOrEmpty(id)) {
            baseRespResult = doSave(param, request);
        } else {
            baseRespResult = doUpdate(param, request);
        }
        Map<String, Object> data = (Map<String, Object>) baseRespResult.getData();
        modelService.deploy(data.get("JE_WORKFLOW_PROCESSINFO_ID").toString());
        return BaseRespResult.successResult(data, "保存并发布成功！");
    }

    /**
     * 删除模型
     */
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String ids = param.getIds();
        modelService.doRemove(dynaBean.getTableCode(), ids);
        return super.doRemove(param, request);
    }

    /**
     * 批量发布流程
     */
    @RequestMapping(value = "/batchDeploy", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult batchDeploy(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String ids = param.getIds();
        modelService.batchDeploy(dynaBean.getTableCode(), ids);
        return BaseRespResult.successResult(null, "流程部署成功！");
    }

    /**
     * 取消流程部署
     */
    @RequestMapping(value = "/unDeploy", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult unDeploy(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String ids = param.getIds();
        modelService.unDeploy(dynaBean.getTableCode(), ids);
        return BaseRespResult.successResult(null, "取消流程部署成功！");
    }

    /**
     * 禁用流程模型
     */
    @RequestMapping(value = "/disabled", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult disabled(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = null;
        return BaseRespResult.successResult(dynaBean);
    }

    /**
     * 启用流程模型
     */
    @RequestMapping(value = "/enable", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult enable(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = null;
        return BaseRespResult.successResult(dynaBean);
    }


}
