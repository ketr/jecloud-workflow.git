/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.controller.procerss;

import com.google.common.base.Strings;
import com.je.bpm.engine.ActivitiException;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.workflow.service.adjustNode.AddNodeService;
import com.je.workflow.service.adjustNode.DelNodeService;
import com.je.workflow.service.adjustNode.vo.AddNodeParamsVo;
import com.je.workflow.service.adjustNode.vo.AdjustSerialUserInfoVo;
import com.je.workflow.service.adjustNode.vo.DelNodeParamsVo;
import com.je.workflow.service.button.ButtonService;
import com.je.workflow.service.model.ModelService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 流程信息控制层
 */

@RestController
@RequestMapping(value = "/je/workflow/button")
public class ButtonController extends AbstractPlatformController {

    @Autowired
    private ButtonService buttonService;
    @Autowired
    private AddNodeService addNodeService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private DelNodeService delNodeService;

    /**
     * 获取流程按钮操作参数详情
     */
    @RequestMapping(value = "/getParams", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getParams(BaseMethodArgument param, HttpServletRequest request) {
        String operationId = getStringParameter(request, "operationId");
        if (Strings.isNullOrEmpty(operationId)) {
            throw new ActivitiException("operationId为空！");
        }
        return BaseRespResult.successResult(buttonService.getParams(operationId));
    }

    /**
     * 执行流程按钮操作
     */
    @RequestMapping(value = "/operate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult operate(BaseMethodArgument param, HttpServletRequest request) {
        //---------------通用参数---------------------------
        String operationId = getStringParameter(request, "operationId");
        String prod = getStringParameter(request, "prod");
        String funcCode = getStringParameter(request, "funcCode");
        String pdid = getStringParameter(request, "pdid");
        String beanId = getStringParameter(request, "beanId");
        String tableCode = getStringParameter(request, "tableCode");
        String funcId = getStringParameter(request, "funcId");
        //--------------节点人员信息参数--------------------------
        String assignee = getStringParameter(request, "assignee");
        buttonService.addCommonUser(assignee);
        //--------------自定义参数--------------------------
        Map<String, Object> operationCustomerParam = buttonService.buildOperationCustomerParam(operationId, request);
        //--------------加签节点和减签节点，实现业务，直接处理--------------------------
        if (operationId.equals("taskAddSignatureNodeOperator")) {
            Map<String, Object> result = new HashedMap();
            addTaskNode(request);
            result.put("processInfo", buttonService.getButtons(prod, tableCode, funcCode, beanId));
            result.put("bean", buttonService.getBean(prod, beanId, tableCode));
            return BaseRespResult.successResult(result);
        }
        if (operationId.equals("taskDelSignatureNodeOperator")) {
            Map<String, Object> result = new HashedMap();
            delTaskNode(request);
            result.put("processInfo", buttonService.getButtons(prod, tableCode, funcCode, beanId));
            result.put("bean", buttonService.getBean(prod, beanId, tableCode));
            return BaseRespResult.successResult(result);
        }

        Map<String, Object> result = buttonService.operate(operationId, prod, funcCode, pdid, beanId, tableCode, funcId, operationCustomerParam);
        return BaseRespResult.successResult(result);
    }

    /**
     * 加签节点
     *
     * @param request
     * @return
     */
    public BaseRespResult addTaskNode(HttpServletRequest request) {
        String beanId = request.getParameter("beanId");
        String pdid = request.getParameter("pdid");
        String sourceId = request.getParameter("sourceId");
        String targetId = request.getParameter("target");
        String taskId = request.getParameter("taskId");
        List<AdjustSerialUserInfoVo> userInfoVoList = AdjustSerialUserInfoVo.buildStringToList(request.getParameter("users"));

        //流程未启动判断
        if (Strings.isNullOrEmpty(sourceId) && Strings.isNullOrEmpty(taskId)) {
            if (metaService.select("act_ru_execution",
                    ConditionsWrapper.builder().eq("BUSINESS_KEY_", beanId).selectColumns("ID_")).size() > 0) {
                throw new RuntimeException("操作异常，该流程已经启动，请退出表单重新进入！");
            }
            sourceId = modelService.getFirstTaskRef(pdid);
        }
        //流程启动判断
        if (Strings.isNullOrEmpty(sourceId) && !Strings.isNullOrEmpty(taskId)) {
            DynaBean dynaBean = metaService.selectOneByPk("act_ru_task", taskId);
            if (dynaBean != null) {
                sourceId = dynaBean.getStr("TASK_DEF_KEY_");
            }
        }

        if (Strings.isNullOrEmpty(sourceId)) {
            throw new RuntimeException("操作异常，请退出表单重新进入！");
        }

        AddNodeParamsVo vo = AddNodeParamsVo.build()
                .pdid(pdid).beanId(beanId).toSourceId(sourceId)
                .toTargetId(targetId).users(userInfoVoList);
        File file = addNodeService.addSerialNode(vo);
        return BaseRespResult.successResult("加签成功！");
    }

    /**
     * 减签节点
     *
     * @param request
     * @return
     */
    public BaseRespResult delTaskNode(HttpServletRequest request) {
        String beanId = request.getParameter("beanId");
        String pdid = request.getParameter("pdid");
        String taskNodeIds = request.getParameter("taskNodeIds");
        DelNodeParamsVo vo = DelNodeParamsVo.build()
                .pdid(pdid).beanId(beanId).taskNodeIds(taskNodeIds);
        File file = delNodeService.delNode(vo);
        return BaseRespResult.successResult("减签成功！");
    }

}
