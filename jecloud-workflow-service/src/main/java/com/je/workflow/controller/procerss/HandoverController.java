package com.je.workflow.controller.procerss;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.rbac.annotation.ControllerAuditLog;
import com.je.workflow.service.handover.HandoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping(value = "/je/workflow/futureProcessHandover")
public class HandoverController extends AbstractPlatformController {

    @Autowired
    private HandoverService handoverService;

    @Override
    @RequestMapping(
            value = {"/load"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "重写流程监控load方法"
    )
    @ControllerAuditLog(moduleName = "流程监控模块", operateTypeCode = "loadWorkflow", operateTypeName = "查看流程监控", logTypeCode = "systemManage", logTypeName = "系统管理")
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getAssigneeValue(param.getjQuery());
        Page page = handoverService.load(userId);
        return page == null ? BaseRespResult.successResultPage(Lists.newArrayList(), 0L)
                : BaseRespResult.successResultPage(page.getRecords(), (long) page.getTotal());
    }


    @RequestMapping(
            value = {"/processHandover"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"},
            name = "流程交接"
    )
    public BaseRespResult processHandover(BaseMethodArgument param, HttpServletRequest request) {
        String assignee = getStringParameter(request, "assignee");
        String toAssignee = getStringParameter(request, "toAssignee");
        String type = getStringParameter(request, "type");
        String ids = getStringParameter(request, "ids");
        if (!Strings.isNullOrEmpty(type) && type.equals("all")) {
            if (Strings.isNullOrEmpty(assignee)) {
                return BaseRespResult.errorResult("请指定待办人员。");
            }
            handoverService.processAllHandover(assignee, toAssignee);
        } else {
            if (Strings.isNullOrEmpty(ids)) {
                return BaseRespResult.errorResult("请指定交接数据。");
            }
            if (Strings.isNullOrEmpty(ids) || Strings.isNullOrEmpty(toAssignee)) {
                return BaseRespResult.errorResult("请指定交接人员。");
            }
            handoverService.processHandoverByIds(assignee, toAssignee, ids);
        }
        return BaseRespResult.successResult("交接成功！");
    }

    private String getAssigneeValue(String jsonString) {
        try {
            JSONObject jsonObject = JSONObject.parseObject(jsonString);
            JSONArray customArray = jsonObject.getJSONArray("custom");
            JSONObject customObject = customArray.getJSONObject(0);
            JSONArray valueArray = customObject.getJSONArray("value");
            JSONObject valueObject = valueArray.getJSONObject(0);
            String assigneeValue = valueObject.getString("value");
            return assigneeValue;
        } catch (Exception e) {
            return "";
        }
    }

}
