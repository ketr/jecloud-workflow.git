/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.rpc;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.AbstractUpgradeModulePackageRpcServiceImpl;
import com.je.common.base.service.rpc.UpgradeModulePackageRpcService;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.upgrade.UpgradelogRpcService;
import com.je.workflow.service.model.ModelService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@RpcSchema(schemaId = "upgradeModulePackageRpcService")
public class UpgradeModulePackageRpcServiceImpl extends AbstractUpgradeModulePackageRpcServiceImpl implements UpgradeModulePackageRpcService {

    @Autowired
    private MetaService metaService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private UpgradelogRpcService upgradelogRpcService;

    private final Logger logger = LoggerFactory.getLogger(UpgradeModulePackageRpcServiceImpl.class);

    @Override
    public PackageResult packageModule(DynaBean upgradeBean) {
        PackageResult packageResult = new PackageResult();
        List<String> workflowIdList = upgradeBean.get("workflowIdList") == null ? new ArrayList<>() : (List<String>) upgradeBean.get("workflowIdList");
        packageResult.setProductWorkflows(packageWorkflow(workflowIdList));
        return packageResult;
    }

    @Override
    @Transactional
    public String installModule(PackageResult packageResult) {
        List<DynaBean> workflows = packageResult.getProductWorkflows();
        if (workflows != null) {
            for (DynaBean workflow : workflows) {
                workflow.table("JE_WORKFLOW_PROCESSINFO");
                String processinfoId = workflow.getStr("JE_WORKFLOW_PROCESSINFO_ID");
                DynaBean workflowDb = metaService.selectOne("JE_WORKFLOW_PROCESSINFO", ConditionsWrapper.builder().eq("JE_WORKFLOW_PROCESSINFO_ID", processinfoId));
                if (workflowDb == null) {
                    workflow.setStr("JE_WORKFLOW_PROCESSINFO_ID", processinfoId);
                    modelService.doSave(workflow, workflow.getStr("metaInfoJson"), workflow.getStr("metaInfoXml"));
                    commonService.buildModelCreateInfo(workflow);
                    workflow.table("JE_WORKFLOW_PROCESSINFO");
                    metaService.insert(workflow);
                } else {
                    String modelId = workflow.getStr("PROCESSINFO_MODEL_ID");
                    String xmlId = workflow.getStr("PROCESSINFO_RESOURCE_XML_ID");
                    DynaBean updateBean = new DynaBean("JE_WORKFLOW_PROCESSINFO", true);
                    updateBean.setStr("JE_WORKFLOW_PROCESSINFO_ID", workflow.getStr("JE_WORKFLOW_PROCESSINFO_ID"));
                    updateBean.setStr("PROCESSINFO_DEPLOY", workflow.getStr("PROCESSINFO_DEPLOY"));
                    updateBean.setStr("SY_PRODUCT_ID", workflow.getStr("SY_PRODUCT_ID"));
                    updateBean.setStr("SY_PRODUCT_CODE", workflow.getStr("SY_PRODUCT_CODE"));
                    updateBean.setStr("SY_PRODUCT_NAME", workflow.getStr("SY_PRODUCT_NAME"));
                    updateBean.setStr("PROCESSINFO_TYPE_CODE", workflow.getStr("PROCESSINFO_TYPE_CODE"));
                    updateBean.setStr("PROCESSINFO_TYPE_NAME", workflow.getStr("PROCESSINFO_TYPE_NAME"));
                    updateBean.setStr("PROCESSINFO_NAME", workflow.getStr("PROCESSINFO_NAME"));
                    updateBean.setStr("PROCESSINFO_KEY", workflow.getStr("PROCESSINFO_KEY"));
                    updateBean.setStr("PROCESSINFO_DEPLOYMENT_ENVIRONMENT", workflow.getStr("PROCESSINFO_DEPLOYMENT_ENVIRONMENT"));
                    updateBean.setStr("PROCESSINFO_DESCRIBE", workflow.getStr("PROCESSINFO_DESCRIBE"));
                    updateBean.setStr("PROCESSINFO_FUNC_CODE", workflow.getStr("PROCESSINFO_FUNC_CODE"));
                    updateBean.setStr("PROCESSINFO_FUNC_NAME", workflow.getStr("PROCESSINFO_FUNC_NAME"));
                    updateBean.setStr("PROCESSINFO_FUNC_ID", workflow.getStr("PROCESSINFO_FUNC_ID"));
                    updateBean.setStr("PROCESSINFO_ATTACHED_FUNC_NAMES", workflow.getStr("PROCESSINFO_ATTACHED_FUNC_NAMES"));
                    updateBean.setStr("PROCESSINFO_ATTACHED_FUNC_CODES", workflow.getStr("PROCESSINFO_ATTACHED_FUNC_CODES"));
                    updateBean.setStr("PROCESSINFO_ATTACHED_FUNC_IDS", workflow.getStr("PROCESSINFO_ATTACHED_FUNC_IDS"));
                    updateBean.setStr("PROCESSINFO_CONTENT_TEMPLATE", workflow.getStr("PROCESSINFO_CONTENT_TEMPLATE"));
                    updateBean.setStr("PROCESSINFO_SAAS_PID", "PROCESSINFO_SAAS_PID");
                    metaService.update(updateBean);
                    modelService.doUpdate(modelId, xmlId, workflow, workflow.getStr("metaInfoJson"), workflow.getStr("metaInfoXml"));
                }
                modelService.deploy(processinfoId, true);
                //记录升级日志
                upgradelogRpcService.saveUpgradelog(packageResult.getInstallPackageId(),
                        UpgradeResourcesEnum.WORKFLOW.getType(), UpgradeResourcesEnum.WORKFLOW.getName(),
                        workflow.getStr("PROCESSINFO_NAME"), workflow.getStr("PROCESSINFO_KEY"));
            }
        }
        return null;
    }

    private List<DynaBean> packageWorkflow(List<String> workflowIds) {
        List<DynaBean> workflowBeanList = metaService.select("JE_WORKFLOW_PROCESSINFO", ConditionsWrapper.builder()
                .in("JE_WORKFLOW_PROCESSINFO_ID", workflowIds));
        for (DynaBean dynaBean : workflowBeanList) {
            String xmlId = dynaBean.getStr("PROCESSINFO_RESOURCE_XML_ID");
            String modelId = dynaBean.getStr("PROCESSINFO_MODEL_ID");
            dynaBean.setStr("PROCESSINFO_DEPLOYMENT_ENVIRONMENT", "SS");
            dynaBean.set("metaInfoXml", modelService.getXmlInfoById(xmlId));
            JSONObject metaInfoJson = JSONObject.parseObject(modelService.getJsonInfoById(modelId));
            metaInfoJson.getJSONObject("properties").getJSONObject("processBasicConfig").put("deploymentEnvironment", "SS");
            dynaBean.set("metaInfoJson", metaInfoJson.toString());
        }
        return workflowBeanList;
    }

    @Override
    public String doCheck(PackageResult packageResult) {
        return null;
    }
}
