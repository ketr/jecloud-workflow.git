package com.je.workflow.interceptor;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.project.Project;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.ProjectContextHolder;
import com.je.meta.rpc.project.ProjectWorkflowRpcService;
import com.je.servicecomb.RpcSchemaFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 流程发起按钮，添加项目权限
 */

@Aspect
@Component
public class ProjectInitiateButtonInterceptor {

    @Autowired
    private MetaResourceService metaResourceService;

    @Pointcut("execution(* com.je.workflow.service.button.ButtonServiceImpl.getButtons(String, String, String, String))")
    public void buttonMethods() {
    }

    @Around("buttonMethods()")
    public Object aroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed();
        Object[] args = joinPoint.getArgs();
        String prod = args[0] != null ? (String) args[0] : "";
        String funcCode = args[2] != null ? (String) args[2] : "";
        if (prod == null || funcCode == null) {
            return result;
        }
        // 修改返回值
        if (result instanceof List) {
            List<Map<String, Object>> modifiedResult = new ArrayList<>((List<Map<String, Object>>) result);
            List<Map<String, Object>> resultMap = updateModifiedResult(modifiedResult, prod, funcCode);
            return resultMap;
        }
        return result;
    }

    private List<Map<String, Object>> updateModifiedResult(List<Map<String, Object>> modifiedResult, String prod, String funcCode) {
        // 如果条件满足，则直接返回不作进一步处理
        if (modifiedResult.size() > 1) {
            return modifiedResult;
        }

        Map<String, Object> workButtonInfo = modifiedResult.get(0);
        JSONObject workButtonInfoJson = new JSONObject(workButtonInfo);

        // 判断流程是否已经启动
        if (!"NOSTATUS".equals(workButtonInfoJson.getJSONObject("workflowConfig").getString("audFlag"))) {
            return modifiedResult;
        }

//                Project project1 = new Project();
//        project1.setCode("XM1");
//        project1.setId("ec3de800ca2941cface6f944bb3ce793");
//        project1.setName("XM1");
//        ProjectContextHolder.setProject(project1);

        Project project = ProjectContextHolder.getProject();
        if (project == null) {
            return modifiedResult;
        }

        DynaBean dynaBean = metaResourceService.selectOneByNativeQuery(
                "JE_RBAC_PROJECTFLOWMAPPING",
                NativeQuery.build().eq("PROJECTFLOWMAPPING_PRODUCT_CODE", prod)
        );
        if (dynaBean == null) {
            return modifiedResult;
        }

        // 从 dynaBean 获取配置信息
        String tableCode = dynaBean.getStr("PROJECTFLOWMAPPING_TABLE_CODE");
        String funcFiledCode = dynaBean.getStr("PROJECTFLOWMAPPING_FUNCCODEFIELD_CODE");
        String wfKeyCode = dynaBean.getStr("PROJECTFLOWMAPPING_LCKEYZD_CODE");
        String projectFiledCode = dynaBean.getStr("PROJECTFLOWMAPPING_PROPKFIELD_CODE");
        String projectId = project.getId();
//        String projectId = "ec3de800ca2941cface6f944bb3ce793";

        ProjectWorkflowRpcService projectWorkflowRpcService = "workflow".equals(prod) ?
                SpringContextHolder.getBean(ProjectWorkflowRpcService.class) :
                RpcSchemaFactory.getRemoteProvierClazz(prod, "projectWorkflowRpcService", ProjectWorkflowRpcService.class);

        List<DynaBean> workflowList = projectWorkflowRpcService.findProjectWorkflow(
                projectId, projectFiledCode, tableCode, funcCode, funcFiledCode
        );

        if (workflowList == null || workflowList.isEmpty()) {
            return modifiedResult;
        }

        // 生成关键字列表
        List<String> keys = workflowList.stream()
                .map(bean -> bean.getStr(wfKeyCode, ""))
                .filter(key -> !Strings.isNullOrEmpty(key))
                .collect(Collectors.toList());

        // 过滤 allModelsInfo 和 buttonList 中不匹配的项
        workButtonInfoJson.put("allModelsInfo", filterJSONArrayByKeys(workButtonInfoJson.getJSONArray("allModelsInfo"), keys));
        workButtonInfoJson.put("buttonList", filterJSONArrayByKeys(workButtonInfoJson.getJSONArray("buttonList"), keys));

        // 将 JSON 数据转为 Map 并返回结果
        Map<String, Object> resultMap = new HashMap<>(workButtonInfoJson);
        List<Map<String, Object>> resultMaps = new ArrayList<>();
        resultMaps.add(resultMap);
        return resultMaps;
    }

    private JSONArray filterJSONArrayByKeys(JSONArray jsonArray, List<String> keys) {
        JSONArray filteredArray = new JSONArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String pdidKey = jsonObject.getString("pdid").split(":")[0];
            if (keys.contains(pdidKey)) {
                filteredArray.add(jsonObject);
            }
        }
        return filteredArray;
    }



}

