/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.esotericsoftware.kryo.serializers;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.objenesis.strategy.StdInstantiatorStrategy;
import org.springframework.stereotype.Component;
import sun.reflect.ReflectionFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Constructor;
import java.util.concurrent.ConcurrentHashMap;

public class SerializeUtils {

    private final ThreadLocal<Kryo> kryoLocal = ThreadLocal.withInitial(() -> {
        // kyro是线程不安全的，需要为每个线程创建一个独立的对象
        Kryox kryo = new Kryox();
        kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(
                new StdInstantiatorStrategy()));
        kryo.setDefaultSerializer(CustomSerializer.class);
        return kryo;
    });

    public byte[] serialize(Object obj) {
        if (obj == null) {
            return null;
        }

        Kryo kryo = kryoLocal.get();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Output output = new Output(byteArrayOutputStream);
        kryo.writeClassAndObject(output, obj);
        output.close();
        return byteArrayOutputStream.toByteArray();
    }


    public Object deSerialize(byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        Kryo kryo = kryoLocal.get();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        Input input = new Input(byteArrayInputStream);
        input.close();
        return kryo.readClassAndObject(input);
    }


    /**
     * Kyro序列化没有无参构造方法的类时会报错，所以进行改造
     */
    class Kryox extends Kryo {

        private final ReflectionFactory REFLECTION_FACTORY = ReflectionFactory.getReflectionFactory();

        private final ConcurrentHashMap<Class<?>, Constructor<?>> _constructors = new ConcurrentHashMap<Class<?>, Constructor<?>>();

        @Override
        public <T> T newInstance(Class<T> type) {
            try {
                return super.newInstance(type);
            } catch (Exception e) {
                return (T) newInstanceFromReflectionFactory(type);
            }
        }

        private Object newInstanceFrom(Constructor<?> constructor) {
            try {
                return constructor.newInstance();
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
        }

        @SuppressWarnings("unchecked")
        public <T> T newInstanceFromReflectionFactory(Class<T> type) {
            Constructor<?> constructor = _constructors.get(type);
            if (constructor == null) {
                constructor = newConstructorForSerialization(type);
                Constructor<?> saved = _constructors.putIfAbsent(type, constructor);
                if (saved != null)
                    constructor = saved;
            }
            return (T) newInstanceFrom(constructor);
        }

        private <T> Constructor<?> newConstructorForSerialization(Class<T> type) {
            try {
                Constructor<?> constructor = REFLECTION_FACTORY.newConstructorForSerialization(type,
                        Object.class.getDeclaredConstructor());
                constructor.setAccessible(true);
                return constructor;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }
}

