package com.je.workflow.rpc.dictionary;

import com.je.workflow.model.EventSubmitDTO;

public interface WorkflowEventRpcService {
    boolean executeCustomMethod(EventSubmitDTO eventSubmitDTO, String beanName,String prod);
}
