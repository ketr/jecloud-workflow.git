/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

public class SubmitOutGoingNodeAssigneeVo {

    private String pdid;
    private String piid;
    private String operationId;
    private String tableCode;
    private String funcCode;
    private String funcId;
    private String prod;
    private String beanId;
    private String target;
    private String taskId;
    private String adjust;

    public String getPdid() {
        return pdid;
    }

    public SubmitOutGoingNodeAssigneeVo setPdid(String pdid) {
        this.pdid = pdid;
        return this;
    }

    public String getPiid() {
        return piid;
    }

    public SubmitOutGoingNodeAssigneeVo setPiid(String piid) {
        this.piid = piid;
        return this;
    }

    public String getOperationId() {
        return operationId;
    }

    public SubmitOutGoingNodeAssigneeVo setOperationId(String operationId) {
        this.operationId = operationId;
        return this;
    }

    public String getTableCode() {
        return tableCode;
    }

    public SubmitOutGoingNodeAssigneeVo setTableCode(String tableCode) {
        this.tableCode = tableCode;
        return this;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public SubmitOutGoingNodeAssigneeVo setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public String getFuncId() {
        return funcId;
    }

    public SubmitOutGoingNodeAssigneeVo setFuncId(String funcId) {
        this.funcId = funcId;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public SubmitOutGoingNodeAssigneeVo setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public SubmitOutGoingNodeAssigneeVo setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public SubmitOutGoingNodeAssigneeVo setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public SubmitOutGoingNodeAssigneeVo setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getAdjust() {
        return adjust;
    }

    public SubmitOutGoingNodeAssigneeVo setAdjust(String adjust) {
        this.adjust = adjust;
        return this;
    }
}
