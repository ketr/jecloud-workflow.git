/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

import java.util.Map;

public class WorkFlowInfoVo {
    /**
     * 是否包含当前登录人
     */
    private Boolean addOwn;
    /**
     * 是否是多人节点
     */
    private Boolean multiple;
    /**
     * 业务bean
     */
    private Map<String, Object> bean;
    /**
     * 当前节点id
     */
    private String nodeId;
    /**
     * 当前节点名称
     */
    private String nodeName;
    /**
     * 操作标识id
     */
    private ExecutionTypeEnum operationId;
    /**
     * 流程启动用户id
     */
    private String starterUser;

    public enum ExecutionTypeEnum {
        TASK_SUBMIT_OPERATOR("taskSubmitOperation", "任务提交"),
        TASK_DELEGATE_OPERATOR("taskDelegateOperation", "任务委托"),
        PROCESS_EMPTY_SPONSOR_OPERATOR("processEmptySponsorOperation", "流程空发起操作"),
        TASK_PERSONNEL_ADJUSTMENTS_OPERATOR("taskPersonnelAdjustmentsOperator", "人员调整"),
        TASK_COUNTERSIGN_OPERATOR("taskCountersignOperator", "任务加签");
        private String name;
        private String value;

        ExecutionTypeEnum(String value, String name) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public static ExecutionTypeEnum getType(String type) {
            if (TASK_SUBMIT_OPERATOR.getValue().equalsIgnoreCase(type) || PROCESS_EMPTY_SPONSOR_OPERATOR.getValue().equalsIgnoreCase(type)) {
                return TASK_SUBMIT_OPERATOR;
            } else if (TASK_DELEGATE_OPERATOR.getValue().equalsIgnoreCase(type)) {
                return TASK_DELEGATE_OPERATOR;
            } else if (TASK_COUNTERSIGN_OPERATOR.getValue().equalsIgnoreCase(type)) {
                return TASK_COUNTERSIGN_OPERATOR;
            } else if (TASK_PERSONNEL_ADJUSTMENTS_OPERATOR.getValue().equalsIgnoreCase(type)) {
                return TASK_PERSONNEL_ADJUSTMENTS_OPERATOR;
            }
            return null;
        }
    }

    public Boolean getMultiple() {
        return multiple;
    }

    public void setMultiple(Boolean multiple) {
        this.multiple = multiple;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Boolean getAddOwn() {
        return addOwn;
    }

    public void setAddOwn(Boolean addOwn) {
        this.addOwn = addOwn;
    }

    public ExecutionTypeEnum getOperationId() {
        return operationId;
    }

    public void setOperationId(ExecutionTypeEnum operationId) {
        this.operationId = operationId;
    }

    public String getStarterUser() {
        return starterUser;
    }

    public void setStarterUser(String starterUser) {
        this.starterUser = starterUser;
    }
}
