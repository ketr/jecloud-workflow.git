package com.je.workflow.exception;

public class ActivitiEventException extends Exception {
    private static final long serialVersionUID = 1L;

    public ActivitiEventException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActivitiEventException(String message) {
        super(message);
    }
}
