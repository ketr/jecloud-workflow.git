package com.je.workflow.rpc;

import com.je.common.base.spring.SpringContextHolder;
import com.je.workflow.model.EventSubmitDTO;
import com.je.workflow.rpc.dictionary.WorkFlowExecuteCustomMethodService;
import com.je.workflow.rpc.dictionary.WorkflowEventRpcService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.apache.servicecomb.provider.pojo.reference.PojoReferenceMeta;

@RpcSchema(schemaId = "workflowEventRpcService")
public class WorkflowEventRpcServiceImpl implements WorkflowEventRpcService {

    @Override
    public boolean executeCustomMethod(EventSubmitDTO eventSubmitDTO, String beanName, String prod) {
        try {
            if (SpringContextHolder.containsBean(beanName)) {
                WorkFlowExecuteCustomMethodService workFlowExecuteCustomMethodService =
                        SpringContextHolder.getBean(beanName);
                workFlowExecuteCustomMethodService.executeCustomMethod(eventSubmitDTO);
            } else {
                WorkFlowExecuteCustomMethodService customMethodService = (WorkFlowExecuteCustomMethodService) getRemoteProvierClazz(prod, beanName);
                customMethodService.executeCustomMethod(eventSubmitDTO);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Object getRemoteProvierClazz(String microserviceName, String schemaId) {
        PojoReferenceMeta pojoReference = new PojoReferenceMeta();
        pojoReference.setMicroserviceName(microserviceName);
        pojoReference.setSchemaId(schemaId);
        pojoReference.setConsumerIntf(WorkFlowExecuteCustomMethodService.class);
        pojoReference.afterPropertiesSet();
        return pojoReference.getProxy();
    }

}
