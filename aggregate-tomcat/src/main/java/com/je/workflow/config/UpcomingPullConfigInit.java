package com.je.workflow.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpcomingPullConfigInit {

    @Bean("UpcomingPullConfigInitlator")
    public void init() {
        Config config = ConfigService.getConfig("upcoming");
        String isPull = config.getProperty("upcoming.pull", "0");
        if (isPull.equals("1")) {
            UpcomingPullConfig.IS_PULL = true;
        }
        UpcomingPullConfig.PROD_NAME = config.getProperty("upcoming.prodName", "");
        UpcomingPullConfig.SERVICE_NAME = config.getProperty("upcoming.serviceName", "");
    }

}
